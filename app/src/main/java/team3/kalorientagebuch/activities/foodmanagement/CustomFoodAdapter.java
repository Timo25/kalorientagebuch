package team3.kalorientagebuch.activities.foodmanagement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Food;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * Custom adapter to fill the listview in the foodmanagement activity with all foods
 */
public class CustomFoodAdapter extends BaseAdapter {

    //class variables
    private Context mContext;
    private List<Food> mList;
    private ClickListener mClickListener;

    // Comparator for sorting the array
    private static final Comparator<Food> comparator = new Comparator<Food>() {
        public int compare(Food food1, Food food2) {
            return food1.toString().compareToIgnoreCase(food2.toString());
        }
    };

    //constructor
    public CustomFoodAdapter(Context context, List<Food> list, ClickListener clickListener){
        mContext = context;
        mList = list;
        mClickListener = clickListener;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Food getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }
    @Override
    public void notifyDataSetChanged() {
        Collections.sort(mList, comparator);
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //Save current item
        Food curItem = getItem(i);

        //Select layout
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.foodmanagement_listview, null);
        }

        //Fill textviews of each item
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        TextView tvCalories = (TextView) view.findViewById(R.id.tvCalories);

        // set texts
        tvName.setText(curItem.toString());
        tvCalories.setText(HelperClass.doubleToString(curItem.getCalories()) + " kcal");

        // set clicklisteners to buttons
        ImageButton btnEdit = (ImageButton)view.findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(mClickListener);
        ImageButton btnDelete = (ImageButton)view.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(mClickListener);

        return view;
    }
}
