package team3.kalorientagebuch.activities.foodmanagement;

import android.widget.Button;
import android.widget.ListView;

import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * Gui class contains references to all Gui elements
 */
public class Gui {
    // link to the Init object extending the Fragment
    private Init mFragment;

    // Gui elements
    private Button mbtnNewUnit;
    private ListView mlvUnits;

    // constructor sets the objects
    public Gui (Init fragment) {
        mFragment = fragment;

        mbtnNewUnit = (Button)mFragment.getView().findViewById(R.id.btnNew);
        mlvUnits = (ListView)mFragment.getView().findViewById(R.id.lv);
    }

    // Gui element getter
    public Button getBtnNew() {
        return mbtnNewUnit;
    }
    public ListView getLV() {
        return mlvUnits;
    }
}
