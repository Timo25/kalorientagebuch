package team3.kalorientagebuch.activities.foodmanagement;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.View;

import java.util.List;

import team3.kalorientagebuch.Helpers.DataStorage;
import team3.kalorientagebuch.Objects.Food;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects
    private team3.kalorientagebuch.activities.hamburger.Init mHamburger;
    private Gui mGui;
    private Init mFragment;

    // list of strings for the listview
    private List<Food> mList;
    private CustomFoodAdapter mAdapter;

    // constructor sets the objects and initializes the GUI and the button listener
    public ApplicationLogic (Gui gui, Init fragment, team3.kalorientagebuch.activities.hamburger.Init hamburger) {
        mGui = gui;
        mFragment = fragment;
        mHamburger = hamburger;
        initGui();
        initListener();
    }


    private void initGui() {
        // change title to current content
        mFragment.getActivity().setTitle(mFragment.getActivity().getString(R.string.hamburgermenu3));

        // set listview
        mAdapter = new CustomFoodAdapter(mFragment.getActivity(), DataStorage.data_object.getFoods(), new ClickListener(this));
        mGui.getLV().setAdapter(mAdapter);
    }

    // notify about changes to the list
    public void reloadList()
    {
        mAdapter.notifyDataSetChanged();
    }

    // creates an object of the ClickListener and bounds it to the new food button
    private void initListener() {
        ClickListener clickListener = new ClickListener(this);
        mGui.getBtnNew().setOnClickListener(clickListener);
    }

    // gets called when the new food button is clicked and runs the activity to create new food
    public void onNewClicked() {
        Intent nextActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.food.Init.class);
        mFragment.getActivity().startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    // gets called by clicking on the edit button of an item
    public void editListViewItem(View view)
    {
        // get the index of the clicked item
        int index = mGui.getLV().getPositionForView(view);

        // next activity
        Intent nextActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.food.Init.class);
        // give parameters to next activity
        nextActivity.putExtra("food", mAdapter.getItem(index));
        // start next activity for result
        mFragment.getActivity().startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    // gets called by clicking on the remove button of an item
    public void removeListViewItem(final View view)
    {
        // ask for removing
        new AlertDialog.Builder(mHamburger)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Lebensmittel löschen")
            .setMessage("Möchten Sie das Lebensmittel wirklich löschen?")
            .setPositiveButton("Ja", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // get the index of the clicked item
                    int index = mGui.getLV().getPositionForView(view);

                    // remove the item
                    DataStorage.data_object.remove(mAdapter.getItem(index));

                    // notify the list
                    mAdapter.notifyDataSetChanged();
                }

            })
            .setNegativeButton("Nein", null)
            .show();
    }
}
