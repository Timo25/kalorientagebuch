package team3.kalorientagebuch.activities.account;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import team3.kalorientagebuch.Helpers.CustomFragment;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * Extends CustomFragment to be included in the hamburger menu
 */
public class Init extends CustomFragment {

    // objects of the classes with the needed functions
    private Gui mGui;
    private ApplicationLogic mApplicationLogic;

    // change the content of the container in the hamburger menu
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_fragment, container, false);
    }
    // the View has to be created before editing in fragments is possible
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initGUI();
        initApplicationLogic();
    }
    // save non-persistent data
    @Override
    public void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    // init objects
    private void initGUI () {
        mGui = new Gui(this);
    }
    private void initApplicationLogic () {
        mApplicationLogic = new ApplicationLogic(mGui, this, mHamburger);
    }
}
