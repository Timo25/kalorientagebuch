package team3.kalorientagebuch.activities.account;

import android.content.Context;
import android.view.View;
import android.widget.Toast;

import team3.kalorientagebuch.Helpers.DataStorage;
import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects
    private team3.kalorientagebuch.activities.hamburger.Init mHamburger;
    private Gui mGui;
    private Init mFragment;
    private Context context;
    private String mName = DataStorage.data_object.getName();
    private String mMaxCalories = DataStorage.data_object.getMaxCalories() + "";

    // constructor sets the objects and initializes the GUI and the button listener
    public ApplicationLogic (Gui gui, Init fragment, team3.kalorientagebuch.activities.hamburger.Init hamburger) {
        mGui = gui;
        mFragment = fragment;
        mHamburger = hamburger;
        context = mFragment.getActivity().getApplicationContext();
        initGui();
        initListener();
    }

    private void initGui() {
        // change title to current content
        mFragment.getActivity().setTitle(mFragment.getActivity().getString(R.string.hamburgermenu5));

        // set text from datastorage
        mGui.getEtName().setText(mName);
        mGui.getEtMaxCalories().setText(mMaxCalories);
    }

    private void initListener() {
        // clicklistener für the save button
        ClickListener clickListener = new ClickListener(this);
        mGui.getBtnSave().setOnClickListener(clickListener);

        // hide/show the 0 on focus/unfocus
        mGui.getEtMaxCalories().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(mGui.getEtMaxCalories().getText().toString().length() <= 0){
                    if (hasFocus)
                    {
                        mGui.getEtMaxCalories().setText("");
                    }
                    else
                    {
                        mGui.getEtMaxCalories().setText("0");
                    }
                }
            }
        });
    }

    // gets called when the save button is clicked
    public void onSaveClicked() {
        // gets the data out of the elements
        mName = mGui.getEtName().getText().toString();
        mMaxCalories = mGui.getEtMaxCalories().getText().toString();

        // default: no error
        boolean bError = false;

        // validation and error show
        if (mName.length() <= 0)
        {
            // error message
            mGui.getEtName().setError("Bitte geben Sie Ihren Namen ein.");
            // at least one error
            bError = true;
        }
        else
        {
            // reset error
            mGui.getEtName().setError(null);
        }
        if (HelperClass.stringToInt(mMaxCalories) <= 0)
        {
            mGui.getEtMaxCalories().setError("Die maximalen Tageskalorien müssen über 0 liegen.");
            bError = true;
        }
        else
        {
            mGui.getEtMaxCalories().setError(null);
        }

        // return for one or more errors
        if (bError)
            return;

        // write new data to datastorage
        DataStorage.data_object.setConfig(mName, HelperClass.stringToInt(mMaxCalories));

        // show success message
        Toast.makeText(context, "Daten gespeichert!", Toast.LENGTH_SHORT).show();
        mHamburger.loadSelection(0);
    }
}
