package team3.kalorientagebuch.activities.account;

import android.widget.Button;
import android.widget.EditText;

import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * Gui class contains references to all Gui elements
 */
public class Gui {
    // link to the Init object extending
    private Init mFragment;

    // Gui elements
    private EditText metName;
    private EditText metMaxCalories;
    private Button mbtnSave;

    // constructor sets the objects
    public Gui (Init fragment) {
        mFragment = fragment;

        metName = (EditText)mFragment.getView().findViewById(R.id.etName);
        metMaxCalories = (EditText)mFragment.getView().findViewById(R.id.etMaxCalories);
        mbtnSave = (Button)mFragment.getView().findViewById(R.id.btnSave);
    }

    // Gui element getter
    public EditText getEtName() { return metName; }
    public EditText getEtMaxCalories() {
        return metMaxCalories;
    }
    public Button getBtnSave() {
        return mbtnSave;
    }
}