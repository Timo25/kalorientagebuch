package team3.kalorientagebuch.activities.diary;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;

import team3.kalorientagebuch.Helpers.CalorieCalculator;
import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Entry;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Timo
 * Custom expandable listview adapter which is used in the diary
 */
public class CustomExpandableListAdapter extends BaseExpandableListAdapter {

    //class variables
    private Context context;
    private List<String> separatorTitles;
    private HashMap<String, List<Entry>> listDetails;
    private ApplicationLogic mApplicationLogic;

    //constructor
    public CustomExpandableListAdapter(Context context, List<String> separatorTitles, HashMap<String, List<Entry>> listDetails, ApplicationLogic mApplicationLogic) {
        this.context = context;
        this.separatorTitles = separatorTitles;
        this.listDetails = listDetails;
        this.mApplicationLogic = mApplicationLogic;
    }

    //returns the child for the given position as an Object
    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.listDetails.get(this.separatorTitles.get(listPosition)).get(expandedListPosition);
    }

    //Returns the position of the child in the list
    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    //This method defines the view of the child item
    @Override
    public View getChildView(int listPosition, final int expandedListPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        //Pass the current entry to the ApplicationLogic to find the entry's calories
        Entry curChild = (Entry) getChild(listPosition, expandedListPosition);
        double entryCalories = CalorieCalculator.caloriesFromEntry(curChild);
        final String expandedListText = getChild(listPosition, expandedListPosition).toString();
        //Set the layout which should be used
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.diary_listview_entry, null);
        }
        //Set the first textview to contain the current entry
        TextView expandedListTextView = (TextView) convertView.findViewById(R.id.tvName);
        expandedListTextView.setText(expandedListText);

        //Set the second textview to contain the entry's calories
        TextView childCalories = (TextView) convertView.findViewById(R.id.tvCalories);
        childCalories.setText(HelperClass.doubleToString(entryCalories) + " kcal");

        //get the 4 buttons of each entry and set the click listener to them
        ImageButton copyButton = (ImageButton) convertView.findViewById(R.id.btnCopy);
        ImageButton upButton = (ImageButton) convertView.findViewById(R.id.btnDiaryUp);
        ImageButton downButton = (ImageButton) convertView.findViewById(R.id.btnDiaryDown);
        ImageButton editButton = (ImageButton) convertView.findViewById(R.id.btnDiaryEdit);
        ImageButton deleteButton = (ImageButton) convertView.findViewById(R.id.btnDiaryDelete);
        ClickListenerChildItems cl = new ClickListenerChildItems(mApplicationLogic, (Entry) getChild(listPosition, expandedListPosition), (String) getGroup(listPosition));
        copyButton.setOnClickListener(cl);
        upButton.setOnClickListener(cl);
        downButton.setOnClickListener(cl);
        editButton.setOnClickListener(cl);
        deleteButton.setOnClickListener(cl);
        return convertView;
    }

    //returns the number of children for a given group
    @Override
    public int getChildrenCount(int listPosition) {
        return this.listDetails.get(this.separatorTitles.get(listPosition)).size();
    }

    //Returns the group for a given index as Object
    @Override
    public Object getGroup(int listPosition) {
        return this.separatorTitles.get(listPosition);
    }

    //Returns the number of groups
    @Override
    public int getGroupCount() {
        return this.separatorTitles.size();
    }

    //returns the index of a group for a given position
    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    //Defines the view of the group item
    @Override
    public View getGroupView(int listPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        //Get the list of entries for the current group to calculate the total day's calories
        List<Entry> entriesOfCurrentGroup = listDetails.get(getGroup(listPosition).toString());
        double groupCalories = CalorieCalculator.caloriesFromDay(entriesOfCurrentGroup);
        String listTitle = getGroup(listPosition).toString();
        //Set the layout which should be used
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.diary_listview_day, null);
        }
        //Set the first textview to contain the group name, which is the date in this case, in bold font
        TextView listTitleTextView = (TextView) convertView.findViewById(R.id.tvDay);
        listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle);

        //Set the second textview to contain the day's calories in bold font
        TextView dayCalories = (TextView) convertView.findViewById(R.id.tvDayCalories);
        dayCalories.setTypeface(null, Typeface.BOLD);
        dayCalories.setText(HelperClass.doubleToString(groupCalories) + " kcal");
        return convertView;
    }

    //default methods which have to be implemented but have no further use in this case
    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }
}
