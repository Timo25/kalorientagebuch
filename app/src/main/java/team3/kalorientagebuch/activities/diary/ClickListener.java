package team3.kalorientagebuch.activities.diary;

import android.view.View;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Timo
 * ClickListener class reacts on button clicks
 */
public class ClickListener implements View.OnClickListener {
    // object to execute application logic methods depending on the event
    private ApplicationLogic mApplicationLogic;

    // constructor assigning the ApplicationLogic object
    public ClickListener(ApplicationLogic applicationLogic) {
        mApplicationLogic = applicationLogic;
    }

    // method gets called on click events and calls methods in the ApplicationLogic object depending on the event
    @Override
    public void onClick(View view) {
        switch ( view.getId() ) {
            case R.id.btnDay:
                mApplicationLogic.onDayClicked();
                break;
            case R.id.btnWeek:
                mApplicationLogic.onWeekClicked();
                break;
            case R.id.btnMonth:
                mApplicationLogic.onMonthClicked();
                break;
            case R.id.btnToday:
                mApplicationLogic.onTodayClicked();
                break;
            case R.id.btnNewEntry:
                mApplicationLogic.onNewEntryClicked();
                break;
        }
    }
}
