package team3.kalorientagebuch.activities.diary;

import android.view.View;
import team3.kalorientagebuch.Objects.Entry;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Timo
 * Special click listener which is used for the 4 buttons of each entry
 */
public class ClickListenerChildItems implements View.OnClickListener {

    //Class variables which will be given to the click listener to pass them to the method it calls
    private ApplicationLogic mApplicationLogic;
    private Entry entry;
    private String groupName;

    //Constructor sets the variables
    public ClickListenerChildItems(ApplicationLogic mApplicationLogic, Entry entry, String groupName) {
        this.mApplicationLogic = mApplicationLogic;
        this.entry = entry;
        this.groupName = groupName;
    }

    //call the method in ApplicationLogic depending on the clicked button
    @Override
    public void onClick(View view) {
        switch(view.getId()){
            case R.id.btnCopy:
                mApplicationLogic.copyListViewItemUp(entry, groupName);
                break;
            case R.id.btnDiaryUp:
                mApplicationLogic.moveListViewItemUp(entry, groupName);
                break;
            case R.id.btnDiaryDown:
                mApplicationLogic.moveListViewItemDown(entry, groupName);
                break;
            case R.id.btnDiaryEdit:
                mApplicationLogic.editListViewItem(entry);
                break;
            case R.id.btnDiaryDelete:
                mApplicationLogic.removeListViewItem(entry, groupName);
                break;
        }
    }
}
