package team3.kalorientagebuch.activities.diary;


import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import team3.kalorientagebuch.Helpers.DataStorage;
import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Entry;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Timo
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects and class variables
    private team3.kalorientagebuch.activities.hamburger.Init mHamburger;
    private Gui mGui;
    private Init mFragment;
    private HashMap<String, List<Entry>> listDetails;
    private List<String> separatorTitles;
    private CustomExpandableListAdapter expandableListAdapter;
    private List<Entry> listOfAllEntries;

    // constructor sets the objects and initializes the GUI and the button listener
    public ApplicationLogic(Gui gui, Init fragment, team3.kalorientagebuch.activities.hamburger.Init hamburger) {
        mGui = gui;
        mFragment = fragment;
        mHamburger = hamburger;
        initGui();
        initListener();
    }
    //Initializes the GUI
    private void initGui() {
        // change title to current content
        mFragment.getActivity().setTitle(mFragment.getActivity().getString(R.string.hamburgermenu2));
        //Gets all entries from the db and pushes them into the listview
        listOfAllEntries = DataStorage.data_object.getEntries();
        initExpandableListView(listOfAllEntries);
    }

    //separate method to initialize the expandable view
    public void initExpandableListView(List<Entry> entries){

        //Sorting the listOfAllEntries
        Collections.sort(entries, new Comparator<Entry>() {
            public int compare(Entry entry1, Entry entry2) {
                return entry1.getDate().compareTo(entry2.getDate());
            }
        });

        //save the dates of each entry as a new section title
        separatorTitles = new ArrayList<String>();
        for(Entry entry: entries){
            String dateStr = HelperClass.dateToString(entry.getDate());
            if(!separatorTitles.contains(dateStr)) {
                separatorTitles.add(dateStr);
            }
        }
        //put all entries of one day into the section map
        listDetails = new HashMap<String, List<Entry>>();
        for(String dateStr: separatorTitles){
            List<Entry> curEntries = new ArrayList<Entry>();
            for(Entry entry: entries){
                String curDateStr = HelperClass.dateToString(entry.getDate());
                if(curDateStr.equals(dateStr)){
                    curEntries.add(entry);
                }
            }
            listDetails.put(dateStr,curEntries);
        }

        //hand the map to the customexpandablelistadapter which creates the listview
        expandableListAdapter = new CustomExpandableListAdapter(mFragment.getActivity(), separatorTitles, listDetails, this);
        mGui.getLV().setAdapter(expandableListAdapter);
    }

    // creates an object of the ClickListener and bounds it to the buttons
    private void initListener() {
        ClickListener cl;
        cl = new ClickListener(this);
        mGui.getBtnDay().setOnClickListener(cl);
        mGui.getBtnWeek().setOnClickListener(cl);
        mGui.getBtnMonth().setOnClickListener(cl);
        mGui.getBtnNewEntry().setOnClickListener(cl);
        mGui.getBtnToday().setOnClickListener(cl);
    }

    //get called when the today button is clicked
    public void onTodayClicked(){
        if(listOfAllEntries.size() == 0){
            return;
        }
        Date today = new Date();
        String todayStr = HelperClass.dateToString(today);
        int todayIndex = 0;
        //find the section of today
        for(int i = 0; i < separatorTitles.size();i++){
            String day = separatorTitles.get(i);
            if(day.equals(todayStr)){
                todayIndex = i;
            }
        }
        //collapse all groups
        int groupCount = expandableListAdapter.getGroupCount();
        for(int i = 0; i < groupCount;i++){
            mGui.getLV().collapseGroup(i);
        }
        //expand today's group and set the view to it
        mGui.getLV().expandGroup(todayIndex);
        mGui.getLV().setSelection(todayIndex);
    }

    // gets called when the next day button is clicked
    public void onDayClicked() {
        int groupCount = expandableListAdapter.getGroupCount();
        if(groupCount == 0){
            return;
        }
        for(int i = 0; i < groupCount;i++){
            mGui.getLV().collapseGroup(i);
        }
        //scrolls down the view 1 section and expands it, if its the last section then expand this instead
        int firstVisibleGroup = mGui.getLV().getFirstVisiblePosition();
        int next = firstVisibleGroup +1;
        if(next<groupCount){
            mGui.getLV().expandGroup(next);
            mGui.getLV().setSelection(next);
        }
        else {
            mGui.getLV().expandGroup(groupCount-1);
            mGui.getLV().setSelection(groupCount-1);
        }
    }

    // gets called when the next week button is clicked
    public void onWeekClicked() {
        int groupCount = expandableListAdapter.getGroupCount();
        if(groupCount == 0){
            return;
        }
        for(int i = 0; i < groupCount;i++){
            mGui.getLV().collapseGroup(i);
        }
        //scrolls down the view 7 sections and expands it, if its the last section then expand this instead
        int firstVisibleGroup = mGui.getLV().getFirstVisiblePosition();
        int next = firstVisibleGroup +7;
        if(next<groupCount){
            mGui.getLV().expandGroup(next);
            mGui.getLV().setSelection(next);
        }
        else{
            mGui.getLV().expandGroup(groupCount-1);
            mGui.getLV().setSelection(groupCount-1);
        }
    }

    //gets called when the next month button is clicked
    public void onMonthClicked(){
        int groupCount = expandableListAdapter.getGroupCount();
        if(groupCount == 0){
            return;
        }
        for(int i = 0; i < groupCount;i++){
            mGui.getLV().collapseGroup(i);
        }
        //scrolls down the view 30 sections and expands it, if its the last section then expand this instead
        int firstVisibleGroup = mGui.getLV().getFirstVisiblePosition();
        int next = firstVisibleGroup +30;
        if(next<groupCount){
            mGui.getLV().expandGroup(next);
            mGui.getLV().setSelection(next);
        }
        else{
            mGui.getLV().expandGroup(groupCount-1);
            mGui.getLV().setSelection(groupCount-1);
        }
    }

    // gets called when the new entry button is clicked and runs the activity to create a new entry
    public void onNewEntryClicked() {
        //Navigation
        Intent startEditActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.entry.Init.class);
        mFragment.getActivity().startActivityForResult(startEditActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    //helper method to find the section index of a given section name
    public int findGroupIndex(String groupName){
        int groupIndex = 0;
        for(int i = 0; i < separatorTitles.size(); i++){
            if(separatorTitles.get(i).equals(groupName)){
                groupIndex = i;
            }
        }
        return groupIndex;
    }

    //gets called when the "Copy" button in an entry is clicked and pushes the entry to the "new entry" activity
    public void copyListViewItemUp(Entry entry, String groupName)
    {
        // create next activity
        Intent nextActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.entry.Init.class);
        // give entry to next activity
        nextActivity.putExtra("entry", entry);
        nextActivity.putExtra("copy", true);
        mFragment.getActivity().startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }
    //gets called when the "Up" button in an entry is clicked and moves the item to the day before, it the day doesn't exists then it will be created
    public void moveListViewItemUp(Entry entry, String groupName)
    {
        //Edit the date of the current entry
        Entry actualEntry = listOfAllEntries.get(listOfAllEntries.indexOf(entry));
        Calendar cal = Calendar.getInstance();
        cal.setTime(actualEntry.getDate());
        cal.add(Calendar.DAY_OF_YEAR,-1);
        Date oneDayBefore= cal.getTime();
        actualEntry.setDate(oneDayBefore);
        DataStorage.data_object.edit(actualEntry.getId(),actualEntry);

        //expand the changed groups
        int groupIndexBefore = findGroupIndex(groupName);
        initExpandableListView(listOfAllEntries);
        int groupIndexAfter = findGroupIndex(groupName);
        if(groupIndexBefore < expandableListAdapter.getGroupCount()){
            if(groupIndexBefore == 0){
                mGui.getLV().expandGroup(groupIndexBefore);
                mGui.getLV().expandGroup(groupIndexBefore+1);
                mGui.getLV().setSelection(groupIndexBefore);
                return;
            }
            else{
                mGui.getLV().expandGroup(groupIndexBefore);
                mGui.getLV().expandGroup(groupIndexBefore-1);
                mGui.getLV().setSelection(groupIndexBefore-1);
                return;
            }
        }
        if(groupIndexAfter != 0){
            mGui.getLV().expandGroup(groupIndexBefore-1);
            mGui.getLV().expandGroup(groupIndexBefore);
            mGui.getLV().setSelection(groupIndexBefore-1);
        }
        else{
            mGui.getLV().setSelection(groupIndexBefore-1);
            mGui.getLV().expandGroup(groupIndexBefore-1);
        }
    }
    //gets called when the "Down" button in an entry is clicked and moves the item to the day after, it the day doesn't exists then it will be created
    public void moveListViewItemDown(Entry entry, String groupName)
    {
        //Change the date of the current entry
        Entry actualEntry = listOfAllEntries.get(listOfAllEntries.indexOf(entry));
        Calendar cal = Calendar.getInstance();
        cal.setTime(actualEntry.getDate());
        cal.add(Calendar.DAY_OF_YEAR,+1);
        Date oneDayAfter= cal.getTime();
        actualEntry.setDate(oneDayAfter);
        DataStorage.data_object.edit(actualEntry.getId(),actualEntry);

        //expand the changed groups
        int groupIndexBefore = findGroupIndex(groupName);
        initExpandableListView(listOfAllEntries);
        int groupIndexAfter = findGroupIndex(groupName);
        if(groupIndexAfter != 0){
            mGui.getLV().expandGroup(groupIndexBefore+1);
            mGui.getLV().expandGroup(groupIndexBefore);
            mGui.getLV().setSelection(groupIndexBefore+1);
        }
        else{
            mGui.getLV().expandGroup(groupIndexBefore);
            mGui.getLV().setSelection(groupIndexBefore);
        }
    }
    //gets called when the "Edit" button in an entry is clicked and opens the activity to edit an entry and passes the entry as a parameter
    public void editListViewItem(Entry entry)
    {
        //create next activity and forward entry
        Intent startEditActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.entry.Init.class);
        startEditActivity.putExtra("entry", entry);
        mFragment.getActivity().startActivityForResult(startEditActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }
    //gets called when the "Remove" button in an entry is clicked and removes the entry from the map and then reloads the listview
    public void removeListViewItem(final Entry entry, final String groupName)
    {
        //recheck deletion
        new AlertDialog.Builder(mHamburger)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Eintrag löschen")
                .setMessage("Möchten Sie den Eintrag wirklich löschen?")
                .setPositiveButton("Ja", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        DataStorage.data_object.remove(entry);
                        listOfAllEntries.remove(entry);
                        int groupIndexBefore = findGroupIndex(groupName);
                        initExpandableListView(listOfAllEntries);
                        int groupIndexAfter = findGroupIndex(groupName);
                        if(groupIndexAfter != 0){
                            mGui.getLV().expandGroup(groupIndexBefore);
                        }
                    }

                })
                .setNegativeButton("Nein", null)
                .show();
    }
    //on update
    public void reloadList(){
            initGui();
    }
}
