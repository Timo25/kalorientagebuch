package team3.kalorientagebuch.activities.diary;

import android.widget.Button;
import android.widget.ExpandableListView;

import team3.kalorientagebuch.activities.R;

/**
 * Created by Timo
 * Gui class contains references to all Gui elements
 */
public class Gui {
    // link to the Init object extending the Fragment
    private Init mFragment;

    // Gui elements
    private Button mbtnDay;
    private Button mbtnWeek;
    private Button mbtnMonth;
    private Button mbtnNewEntry;
    private Button mbtnToday;

    private ExpandableListView mlvEntries;

    // constructor sets the objects
    public Gui(Init fragment) {
        mFragment = fragment;

        mbtnDay = (Button) mFragment.getView().findViewById(R.id.btnDay);
        mbtnWeek = (Button) mFragment.getView().findViewById(R.id.btnWeek);
        mbtnMonth = (Button) mFragment.getView().findViewById(R.id.btnMonth);
        mbtnNewEntry = (Button) mFragment.getView().findViewById(R.id.btnNewEntry);
        mbtnToday = (Button) mFragment.getView().findViewById(R.id.btnToday);

        mlvEntries = (ExpandableListView) mFragment.getView().findViewById(R.id.lvDiary);
    }

    // Gui element getter
    public Button getBtnDay() {
        return mbtnDay;
    }

    public Button getBtnWeek() {
        return mbtnWeek;
    }

    public Button getBtnMonth() { return mbtnMonth; }

    public Button getBtnToday() { return mbtnToday; }

    public Button getBtnNewEntry() { return mbtnNewEntry; }

    public ExpandableListView getLV() {
        return mlvEntries;
    }
}
