package team3.kalorientagebuch.activities.entry;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import team3.kalorientagebuch.Objects.Food;

/**
 * Created by Timo
 * creates the new activity
 */
public class Init extends AppCompatActivity{

    // objects of the needed functions
    private Data mData;
    private Gui mGui;
    private ApplicationLogic mApplicationLogic;

    // initialize objects on creation
    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGUI();
        initData(savedInstanceState);
        initApplicationLogic();
    }
    private void initData (Bundle savedInstanceState) {
        mData = new Data(savedInstanceState, this, mGui);
    }
    private void initGUI () {
        mGui = new Gui(this);
    }
    private void initApplicationLogic () {
        mApplicationLogic = new ApplicationLogic(mData, mGui, this);
    }

    // save non-persistent data
    @Override
    protected void onSaveInstanceState (Bundle outState) {
        mData.saveDataInBundle(outState);
        super.onSaveInstanceState(outState);
    }

    //start the activity with a result, in this case the food object which comes from "new food" activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE)
        {
            mApplicationLogic.setFood((Food)data.getSerializableExtra("food"));
        }
    }
}

