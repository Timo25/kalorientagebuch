package team3.kalorientagebuch.activities.entry;

import android.view.View;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Timo
 * ClickListener class reacts on button clicks
 */
public class ClickListener implements View.OnClickListener {
    // object to execute application logic methods depending on the event
    private ApplicationLogic mApplicationLogic;

    // constructor assigning the ApplicationLogic object
    public ClickListener(ApplicationLogic applicationLogic) {
        mApplicationLogic = applicationLogic;
    }

    // method gets called on click events and calls methods in the ApplicationLogic object depending on the event
    @Override
    public void onClick(View view) {
        switch ( view.getId() ) {
            case R.id.btnNewFood:
                mApplicationLogic.onFoodClicked();
                break;
            case R.id.btnSaveEntry:
                mApplicationLogic.onSaveClicked();
                break;
            case R.id.etDate:
                //Special case which opens the date picker dialog, needs the view in which it is called
                mApplicationLogic.showDatePickerDialog(view);
                break;
        }
    }
}
