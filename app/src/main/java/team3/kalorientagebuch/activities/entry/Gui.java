package team3.kalorientagebuch.activities.entry;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Christopher
 * Gui class contains references to all Gui elements
 */
public class Gui {
    // link to the Init object extending the activity
    private Init mActivity;

    // Gui elements
    private EditText metDate;
    private EditText metName;
    private Spinner mspUnit;
    private EditText metQuantity;

    private Button mbtnNewFood;
    private Button mbtnSave;

    private TextView mTvCaloriesEntryAmount;

    // constructor sets the objects
    public Gui(Init activity) {
        mActivity = activity;
        mActivity.setContentView(R.layout.entry_activity);

        metDate = (EditText) mActivity.findViewById(R.id.etDate);
        metName = (EditText) mActivity.findViewById(R.id.etName);
        mbtnNewFood = (Button) mActivity.findViewById(R.id.btnNewFood);
        mspUnit = (Spinner) mActivity.findViewById(R.id.spUnit);
        metQuantity = (EditText) mActivity.findViewById(R.id.etQuantity);
        mbtnSave = (Button) mActivity.findViewById(R.id.btnSaveEntry);

        mTvCaloriesEntryAmount = (TextView) mActivity.findViewById(R.id.tvCaloriesEntryAmount);
    }

    // Gui element getter

    public EditText getMetDate() {
        return metDate;
    }

    public EditText getMetName() {
        return metName;
    }

    public Spinner getMspUnit() {
        return mspUnit;
    }

    public EditText getMetQuantity() {
        return metQuantity;
    }

    public Button getMbtnNewFood() {
        return mbtnNewFood;
    }

    public Button getMbtnSave() {
        return mbtnSave;
    }

    public TextView getmTvCaloriesEntryAmount() {
        return mTvCaloriesEntryAmount;
    }
}