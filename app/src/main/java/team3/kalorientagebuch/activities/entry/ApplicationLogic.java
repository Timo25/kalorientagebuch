package team3.kalorientagebuch.activities.entry;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Toast;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import team3.kalorientagebuch.Helpers.CalorieCalculator;
import team3.kalorientagebuch.Helpers.DataStorage;
import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Entsprechung;
import team3.kalorientagebuch.Objects.Food;
import team3.kalorientagebuch.Objects.Unit;

/**
 * Created by Timo
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects and class variables
    private Data mData;
    private Gui mGui;
    private Init mActivity;
    private ArrayAdapter<Entsprechung> dataAdapter;

    //Constructor assigning objects and executes further initializing methods
    public ApplicationLogic(Data data, Gui gui, Init activity) {
        mData = data;
        mGui = gui;
        mActivity = activity;
        initGui();
        initListener();
    }

    //Initializes the gui also depending on the call it might prefill values to edit an entry
    private void initGui() {
            //Init the spinner for unit choosing
            initSpinner();
        if (mData.getChange()) {
            //Change title
            mActivity.setTitle("Eintrag bearbeiten");
            //Disable the food entry field and the new food button, because we are editing an entry
            mGui.getMetName().setEnabled(false);
            mGui.getMbtnNewFood().setEnabled(false);
        } else {
            //Change title
            mActivity.setTitle("Neuer Eintrag");
            if (mData.getCopy())
            {
                //Disable the food entry field and the new food button, because we are copying an entry
                mGui.getMetName().setEnabled(false);
                mGui.getMbtnNewFood().setEnabled(false);
            }
        }
        //Prefill values, if there are any
        mGui.getMetDate().setText(HelperClass.dateToString(mData.getEntry().getDate()));
        mGui.getMetName().setText(mData.getEntry().getFood().getName());
        mGui.getMetQuantity().setText(String.valueOf(mData.getEntry().getQuantity()));
        //Calculate the current entries calories
        calculateTotalCalories();
    }

    //Inits the spinner for the unit choosing
    public void initSpinner() {
        if (mData.getEntry().getFood() != null)
        {
            //If there is a food in the entry already, then only show the available units for this food in the spinner
            dataAdapter = new ArrayAdapter<Entsprechung>(mActivity, android.R.layout.simple_spinner_item, mData.getEntry().getFood().getEntsprechungen());
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            mGui.getMspUnit().setAdapter(dataAdapter);
            mGui.getMspUnit().setSelection(dataAdapter.getPosition(mData.getEntry().getEntsprechung()));
        }
        else
        {
            //If there is no food available yet, show no units in the spinner
            dataAdapter.clear();
        }
    }

    // creates an object of the ClickListener and bounds it to the buttons
    private void initListener() {
        ClickListener clickListener = new ClickListener(this);
        mGui.getMbtnNewFood().setOnClickListener(clickListener);
        mGui.getMbtnSave().setOnClickListener(clickListener);
        mGui.getMetDate().setOnClickListener(clickListener);

        //Incase the quantity of the entry is changed the total calories will be calculated immediately
        mGui.getMetQuantity().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                calculateTotalCalories();
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
        //Incase a food is entered, the unit spinner will get filled, and the calories will be calculated
        mGui.getMetName().addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void afterTextChanged(Editable editable) {
                mData.getEntry().setFood(DataStorage.data_object.getFoodByName(mGui.getMetName().getText().toString()));
                initSpinner();
                mGui.getMspUnit().setSelection(0);
                calculateTotalCalories();
            }
        });
        //If the focus of the quantity field changes and there is no value in it, then write "0.0"
        mGui.getMetQuantity().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (HelperClass.stringToDouble(mGui.getMetQuantity().getText().toString()) <= 0) {
                    if(!mData.getChange()){
                        if (hasFocus) {
                            mGui.getMetQuantity().setText("");
                        } else {
                            mGui.getMetQuantity().setText("0.0");
                        }
                    }
                }
            }
        });
    }

    // initialize food if we enter the activity from the "new food" activity
    public void setFood(Food food)
    {
        mData.getEntry().setFood(food);
        mGui.getMetName().setText(mData.getEntry().getFood().getName());
        initSpinner();
        calculateTotalCalories();
    }

    //Datepicker dialog, gets called when the field to choose a date is clicked
    public void showDatePickerDialog(View v) {
        Calendar mcurrentDate = Calendar.getInstance();
        int mYear = mcurrentDate.get(Calendar.YEAR);
        int mMonth = mcurrentDate.get(Calendar.MONTH);
        int mDay = mcurrentDate.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog mDatePicker = new DatePickerDialog(mActivity, new DatePickerDialog.OnDateSetListener() {
            public void onDateSet(DatePicker datepicker, int selectedyear, int selectedmonth, int selectedday) {
                String dateStr = new StringBuilder().append(selectedday).append(".").append(selectedmonth + 1).append(".").append(selectedyear).toString();
                Date date = HelperClass.stringToDate(dateStr);
                mGui.getMetDate().setText(HelperClass.dateToString(date));
            }
        }, mYear, mMonth, mDay);
        mDatePicker.setTitle("Select date");
        mDatePicker.show();
    }

    //Calculates the total calories of an entry
    public void calculateTotalCalories() {
        //Creates an entry object according to the entered values and calculates the calories by using the functions in the CalorieCalculator class
        if (mData.getEntry().getFood() == null ||
                mGui.getMspUnit().getSelectedItem() == null)
        {
            mGui.getmTvCaloriesEntryAmount().setText("0.0 kcal");
            return;
        }
        if (mGui.getMspUnit().getSelectedItem() != null)
        {
            mData.getEntry().setUnit(((Entsprechung)mGui.getMspUnit().getSelectedItem()).getUnit());
        }
        mData.getEntry().setQuantity(HelperClass.stringToDouble(mGui.getMetQuantity().getText().toString()));

        double entryCalories = CalorieCalculator.caloriesFromEntry(mData.getEntry());
        mGui.getmTvCaloriesEntryAmount().setText(HelperClass.doubleToString(entryCalories) + " kcal");
    }

    //Gets executed when user clicks on "New Food" - navigates to the activity to add food
    public void onFoodClicked() {
        Intent nextActivity = new Intent(mActivity, team3.kalorientagebuch.activities.food.Init.class);
        mActivity.startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    //Saves the entered entry incase all fields are filled
    public void onSaveClicked() {
        //Field validation
        boolean error = false;
        String dateStr = mGui.getMetDate().getText().toString();
        if (dateStr.length() <= 0) {
            mGui.getMetDate().setError("Bitte geben Sie ein Datum ein.");
            error = true;
        } else {
            Date date = HelperClass.stringToDate(dateStr);
            if (date == null) {
                mGui.getMetDate().setError("Bitte geben Sie ein Datum im Format 'TT.MM.JJJJ' ein.");
                error = true;
            } else {
                mData.getEntry().setDate(date);
            }
        }
        String nameStr = mGui.getMetName().getText().toString();
        if (nameStr.length() <= 0) {
            mGui.getMetName().setError("Bitte geben Sie ein Lebensmittel ein.");
            error = true;
        } else {
            Food food = DataStorage.data_object.getFoodByName(nameStr);
            if (food == null) {
                mGui.getMetName().setError("Lebensmittel nicht gefunden.");
                error = true;
            } else {
                mData.getEntry().setFood(food);
            }
        }

        if(mGui.getMspUnit().getSelectedItem() == null){
            mGui.getMetName().setError("Bitte geben sie ein Lebensmittel ein.");
            error = true;
        }
        else{
            String unitStr = mGui.getMspUnit().getSelectedItem().toString();
            if(unitStr.length() <= 0){
                mGui.getMetName().setError("Bitte geben sie ein Lebensmittel ein.");
                error = true;
            }
            else{
                Unit unit = DataStorage.data_object.getUnitByName(unitStr);
                List<Entsprechung> list = mData.getEntry().getFood().getEntsprechungen();
                boolean entsprechung = false;
                for (Entsprechung ent : list) {
                    if (ent.getUnit().equals(unit)) {
                        entsprechung = true;
                    }
                }
                if (entsprechung) {
                    mData.getEntry().setUnit(unit);
                } else {
                    mGui.getMetQuantity().setError("Keine Entsprechung mit dieser Einheit vorhanden.");
                    error = true;
                }
            }
        }
        String qtyStr = mGui.getMetQuantity().getText().toString();
        if (qtyStr.length() <= 0) {
            mGui.getMetQuantity().setError("Bitte geben Sie eine Menge an.");
            error = true;
        } else {
            double qty = HelperClass.stringToDouble(qtyStr);
            if (qty == 0) {
                mGui.getMetQuantity().setError("Bitte geben Sie eine positive Dezimalzahl als Menge ein.");
                error = true;
            } else {
                mData.getEntry().setQuantity(qty);
            }
        }
        //Save the entry
        if (!error) {
            if(mData.getChange()){
                DataStorage.data_object.edit(mData.getEntry().getId(), mData.getEntry());
            }
            else{
                DataStorage.data_object.add(mData.getEntry());
            }
            Toast.makeText(mActivity.getApplicationContext(), "Eintrag gespeichert!", Toast.LENGTH_SHORT).show();

            Intent data = new Intent();
            data.putExtra("entry", mData.getEntry());
            mActivity.setResult(team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE, data);
            mActivity.finish();
        }
    }

}