package team3.kalorientagebuch.activities.entry;

import android.os.Bundle;

import java.util.Date;

import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Entry;
import team3.kalorientagebuch.Objects.Entsprechung;
import team3.kalorientagebuch.Objects.Food;

/**
 * Created by Timo
 * Data class contains, stores and restores non-persistent data
 */
public class Data {

    // link to the objects
    private Init mActivity;
    private Gui mGui;

    //non-persistent data to be stored
    private Entry mEntry;
    private boolean mbChange;
    private boolean mbCopy;

    //init or restore data
    public Data (Bundle savedInstanceState, Init activity, Gui gui) {
        mActivity = activity;
        mGui = gui;

        if ( savedInstanceState == null ) {
            Entry intentFood = (Entry)mActivity.getIntent().getSerializableExtra("entry");

            // check whether an entry gets added or edited
            if (intentFood == null)
            {
                mbCopy = false;
                mbChange = false;
                mEntry = new Entry();
            }
            else
            {
                mbCopy = mActivity.getIntent().getBooleanExtra("copy", false);
                mbChange = !mbCopy;
                mEntry = new Entry(intentFood);
                // check for copying
                if (mbCopy)
                {
                    mEntry.setDate(new Date());
                }
            }
        }
        else {
            restoreDataFromBundle(savedInstanceState);
        }
    }

    // save the non-persistent data in the bundle
    public void saveDataInBundle (Bundle b) {
        mEntry.setQuantity(HelperClass.stringToInt(mGui.getMetQuantity().getText().toString()));
        mEntry.setDate(HelperClass.stringToDate(mGui.getMetDate().getText().toString()));
        Food food = new Food();
        food.setName(mGui.getMetName().getText().toString());
        mEntry.setFood(food);
        if (mGui.getMspUnit().getSelectedItem() != null)
        {
            mEntry.setUnit(((Entsprechung)mGui.getMspUnit().getSelectedItem()).getUnit());
        }

        b.putSerializable("entry", mEntry);
        b.putBoolean("change", mbChange);
        b.putBoolean("copy", mbCopy);
    }

    // restore the data of the bundle
    private void restoreDataFromBundle (Bundle b) {
        mEntry = (Entry)b.getSerializable("entry");
        mbChange = b.getBoolean("change");
        mbCopy = b.getBoolean("copy");
    }

    // getter & setter
    public boolean getChange() { return mbChange; }
    public boolean getCopy() { return mbCopy; }
    public Entry getEntry() { return mEntry; }
}
