package team3.kalorientagebuch.activities.unit;

import android.os.Bundle;

import team3.kalorientagebuch.Objects.Unit;

/**
 * Created by Christopher
 * Data class contains, stores and restores non-persistent data
 */
public class Data {

    // link to the objects
    private Init mActivity;
    private Gui mGui;

    //non-persistent data to be stored
    private Unit mUnit;
    private boolean mbChange;
    private String msIDBefore;

    // init or restore data
    public Data (Bundle savedInstanceState, Init activity, Gui gui) {
        mActivity = activity;
        mGui = gui;

        if ( savedInstanceState == null ) {
            Unit intentUnit = (Unit)mActivity.getIntent().getSerializableExtra("unit");

            // check whether a unit gets added or edited
            if (intentUnit == null)
            {
                mbChange = false;
                mUnit = new Unit();
                msIDBefore = "";
            }
            else
            {
                mbChange = true;
                mUnit = new Unit(intentUnit);
                msIDBefore = intentUnit.getName();
            }
        }
        else {
            // restore data if existing
            restoreDataFromBundle(savedInstanceState);
        }
    }

    // save the non-persistent data in the bundle
    public void saveDataInBundle (Bundle b) {
        mUnit.setName(mGui.getEtUnit().getText().toString());
        b.putSerializable("unit", mUnit);
        b.putBoolean("change", mbChange);
        b.putString("idbefore", msIDBefore);
    }

    // restore the data of the bundle
    private void restoreDataFromBundle (Bundle b)
    {
        mUnit = (Unit)b.getSerializable("unit");
        mbChange = b.getBoolean("change");
        msIDBefore = b.getString("idbefore", msIDBefore);
    }

    // getter & setter
    public void setUnit(Unit unit) { mUnit = unit; }
    public Unit getUnit() { return mUnit; }
    public boolean getChange() { return mbChange; }
    public String getIDBefore() { return msIDBefore; }
}
