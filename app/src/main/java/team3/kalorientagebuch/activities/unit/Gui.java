package team3.kalorientagebuch.activities.unit;

import android.widget.Button;
import android.widget.EditText;

import team3.kalorientagebuch.activities.R;

/**
 * Created by Saskia
 * Gui class contains references to all Gui elements
 */
public class Gui {

    // link to the Init object extending the Activity
    private Init mActivity;

    // Gui elements
    private EditText metUnit;
    private Button mbtnSave;

    // constructor sets the objects
    public Gui (Init activity) {
        mActivity = activity;

        mActivity.setContentView(R.layout.unit_activity);

        metUnit = (EditText)mActivity.findViewById(R.id.etUnit);
        mbtnSave = (Button)mActivity.findViewById(R.id.btnSave);
    }

    // Gui element getter
    public EditText getEtUnit() {
        return metUnit;
    }
    public Button getBtnSave() {
        return mbtnSave;
    }
}
