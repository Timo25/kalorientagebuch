package team3.kalorientagebuch.activities.unit;

import android.content.Intent;
import android.widget.Toast;

import team3.kalorientagebuch.Helpers.DataStorage;

/**
 * Created by Saskia
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects
    private Data mData;
    private Gui mGui;
    private Init mActivity;

    // constructor sets the objects and initializes the GUI and the button listener
    public ApplicationLogic (Data data, Gui gui, Init activity) {
        mData = data;
        mGui = gui;
        mActivity = activity;
        initGui();
        initListener();
    }

    private void initGui() {
        // change title to current content
        if (mData.getChange())
        {
            mActivity.setTitle("Einheit bearbeiten");
        }
        else
        {
            mActivity.setTitle("Einheit erstellen");
        }

        // set text to the value of the data object
        mGui.getEtUnit().setText(mData.getUnit().getName());
    }

    // creates an object of the ClickListener and bounds it to the menu
    private void initListener() {
        ClickListener clickListener = new ClickListener(this);
        mGui.getBtnSave().setOnClickListener(clickListener);
    }

    // save button clicked
    public void onSaveClicked() {
        // get the value of the gui object
        mData.getUnit().setName(mGui.getEtUnit().getText().toString());

        // validation and error show
        if (mData.getUnit().getName().length() == 0)
        {
            // error message
            mGui.getEtUnit().setError("Bitte geben Sie einen Namen ein.");
            // at least one error
            return;
        }
        else
        {
            // reset error
            mGui.getEtUnit().setError(null);
        }

        // save the object
        boolean bSuccess = true;
        if (mData.getChange())
        {
            DataStorage.data_object.edit(mData.getIDBefore(), mData.getUnit());
        }
        else
        {
            bSuccess = DataStorage.data_object.add(mData.getUnit());
        }


        if (bSuccess)
        {
            // save the unit an return it to the last activity
            Toast.makeText(mActivity.getApplicationContext(), "Einheit gespeichert!", Toast.LENGTH_SHORT).show();
            Intent data = new Intent();
            data.putExtra("unit", mData.getUnit());
            mActivity.setResult(team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE, data);
            mActivity.finish();
        }
        else
        {
            // error message
            Toast.makeText(mActivity.getApplicationContext(), "Einheit existiert bereits!", Toast.LENGTH_SHORT).show();
        }
    }
}
