package team3.kalorientagebuch.activities.unit;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by Saskia
 * the Unit activity contains the form of adding a unit
 */
public class Init extends AppCompatActivity {

    // objects of the needed functions
    private Data mData;
    private Gui mGui;
    private ApplicationLogic mApplicationLogic;

    // initialize objects on creation
    @Override
    public void onCreate (Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initGUI();
        initData(savedInstanceState);
        initApplicationLogic();
    }

    // save non-persistent data
    @Override
    protected void onSaveInstanceState (Bundle outState) {
        mData.saveDataInBundle(outState);
        super.onSaveInstanceState(outState);
    }

    // init objects
    private void initData (Bundle savedInstanceState) {
        mData = new Data(savedInstanceState, this, mGui);
    }
    private void initGUI () {
        mGui = new Gui(this);
    }
    private void initApplicationLogic () {
        mApplicationLogic = new ApplicationLogic(mData, mGui, this);
    }
}
