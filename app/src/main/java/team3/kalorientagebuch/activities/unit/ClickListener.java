package team3.kalorientagebuch.activities.unit;

import android.view.View;

import team3.kalorientagebuch.activities.R;

/**
 * Created by Saskia
 * ClickListener class reacts on button clicks
 */
public class ClickListener implements View.OnClickListener {
    // object to execute application logic methods depending on the event
    private ApplicationLogic mApplicationLogic;

    // constructor assigning the ApplicationLogic object
    public ClickListener(ApplicationLogic applicationLogic) {
        mApplicationLogic = applicationLogic;
    }

    // method gets called on click events and calls methods in the ApplicationLogic object depending on the event
    @Override
    public void onClick(View view) {
        switch ( view.getId() ) {
            case R.id.btnSave:
                mApplicationLogic.onSaveClicked();
                break;
        }
    }
}
