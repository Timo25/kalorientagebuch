package team3.kalorientagebuch.activities.home;

import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * Gui class contains references to all Gui elements
 */
public class Gui {
    // link to the Init object extending the Fragment
    private Init mFragment;

    // Gui elements
    private TextView mtvKcalToday;
    private TextView mtvKcalMax;
    private TextView mtvKcalDays7;
    private TextView mtvKcalDays14;
    private TextView mtvKcalDays28;

    private Button mbtnNewEntry;
    private Button mbtnDiary;

    private ListView mlvEntriesToday;

    // constructor sets the objects
    public Gui (Init fragment) {
        mFragment = fragment;

        mtvKcalToday = (TextView)mFragment.getView().findViewById(R.id.tvKcalToday);
        mtvKcalMax = (TextView)mFragment.getView().findViewById(R.id.tvKcalMax);
        mtvKcalDays7 = (TextView)mFragment.getView().findViewById(R.id.tvKcalDays7);
        mtvKcalDays14 = (TextView)mFragment.getView().findViewById(R.id.tvKcalDays14);
        mtvKcalDays28 = (TextView)mFragment.getView().findViewById(R.id.tvKcalDays28);

        mbtnNewEntry = (Button)mFragment.getView().findViewById(R.id.btnNew);
        mbtnDiary = (Button)mFragment.getView().findViewById(R.id.btnDiary);

        mlvEntriesToday = (ListView)mFragment.getView().findViewById(R.id.lv);
    }

    // Gui element getter
    public TextView getTVKcalToday() {
        return mtvKcalToday;
    }
    public TextView getTVKcalMax() {
        return mtvKcalMax;
    }
    public TextView getTVKcalDays7() {
        return mtvKcalDays7;
    }
    public TextView getTVKcalDays14() {
        return mtvKcalDays14;
    }
    public TextView getTVKcalDays28() {
        return mtvKcalDays28;
    }
    public Button getBtnNew() {
        return mbtnNewEntry;
    }
    public Button getBtnDiary() {
        return mbtnDiary;
    }
    public ListView getLV() {
        return mlvEntriesToday;
    }
}
