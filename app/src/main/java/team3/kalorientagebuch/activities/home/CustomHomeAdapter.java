package team3.kalorientagebuch.activities.home;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import team3.kalorientagebuch.Helpers.CalorieCalculator;
import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Entry;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * Custom adapter to fill the listview in the home activity with the entries of the current date
 */
//
public class CustomHomeAdapter extends BaseAdapter {

    //class variables
    private Context mContext;
    private List<Entry> mList;
    private ClickListener mClickListener;

    //constructor
    public CustomHomeAdapter(Context context, List<Entry> list, ClickListener clickListener){
        mContext = context;
        mList = list;
        mClickListener = clickListener;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Entry getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //Save current item
        Entry curItem = getItem(i);

        //Select layout
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.home_listview, null);
        }

        //Fill textviews of each item
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        TextView tvCalories = (TextView) view.findViewById(R.id.tvCalories);

        // set texts
        tvName.setText(curItem.toString());
        double calories = CalorieCalculator.caloriesFromEntry(curItem);
        tvCalories.setText(HelperClass.doubleToString(calories) + " kcal");

        // set clicklisteners to buttons
        ImageButton btnCopy = (ImageButton)view.findViewById(R.id.btnCopy);
        btnCopy.setOnClickListener(mClickListener);
        ImageButton btnEdit = (ImageButton)view.findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(mClickListener);
        ImageButton btnDelete = (ImageButton)view.findViewById(R.id.btnDelete);
        btnDelete.setOnClickListener(mClickListener);

        return view;
    }
}
