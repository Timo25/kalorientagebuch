package team3.kalorientagebuch.activities.home;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.view.View;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import team3.kalorientagebuch.Helpers.CalorieCalculator;
import team3.kalorientagebuch.Helpers.DataStorage;
import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Entry;

/**
 * Created by Jonas
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects
    private team3.kalorientagebuch.activities.hamburger.Init mHamburger;
    private Gui mGui;
    private Init mFragment;
    private CustomHomeAdapter mAdapter;
    private DecimalFormat decimalFormat = new DecimalFormat("#.##");

    // constructor sets the objects and initializes the GUI and the button listener
    public ApplicationLogic (Gui gui, Init fragment, team3.kalorientagebuch.activities.hamburger.Init hamburger) {
        mGui = gui;
        mFragment = fragment;
        mHamburger = hamburger;
        initGui();
        initListener();
    }

    private void initGui() {
        // change title to current content
        mFragment.getActivity().setTitle("Kalorientagebuch von " + DataStorage.data_object.getName());

        // set texts
        Date date = new Date();
        String dateStr = HelperClass.dateToString(date);
        double caloriesToday = CalorieCalculator.caloriesFromDay(DataStorage.data_object.getEntries(HelperClass.stringToDate(dateStr)));
        mGui.getTVKcalToday().setText(decimalFormat.format(caloriesToday) + " kcal");
        mGui.getTVKcalMax().setText(String.valueOf(DataStorage.data_object.getMaxCalories()) + " kcal");
        mGui.getTVKcalDays7().setText(decimalFormat.format(avgCaloriesForDays(7)) + " kcal");
        mGui.getTVKcalDays14().setText(decimalFormat.format(avgCaloriesForDays(14)) + " kcal");
        mGui.getTVKcalDays28().setText(decimalFormat.format(avgCaloriesForDays(28)) + " kcal");

        // display red calories today if it is higher than the max calories
        if (DataStorage.data_object.getMaxCalories() < caloriesToday)
        {
            mGui.getTVKcalToday().setTextColor(Color.rgb(200,0,0));
        }
        else
        {
            mGui.getTVKcalToday().setTextColor(mGui.getTVKcalDays28().getTextColors().getDefaultColor());
        }

        loadEntriesToday();
    }

    public void loadEntriesToday()
    {
        // set the list with today's entries
        Date date = new Date();
        String dateStr = HelperClass.dateToString(date);
        mAdapter = new CustomHomeAdapter(mFragment.getActivity(), DataStorage.data_object.getEntries(HelperClass.stringToDate(dateStr)), new ClickListener(this));
        mGui.getLV().setAdapter(mAdapter);
    }

    // creates an object of the ClickListener and bounds it to the buttons
    private void initListener() {
        ClickListener clickListener = new ClickListener(this);
        mGui.getBtnDiary().setOnClickListener(clickListener);
        mGui.getBtnNew().setOnClickListener(clickListener);
    }

    // gets called when the new entry button is clicked and runs the activity to create a new entry
    public void onNewClicked() {
        // create and start next activity
        Intent nextActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.entry.Init.class);
        mFragment.getActivity().startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    // gets called when the diary button is clicked
    public void onDiaryClicked() {
        mHamburger.loadSelection(1); //load Diary
    }


    // gets called by clicking on the copy button of an item
    public void copyListViewItem(View view)
    {
        // get the index of the clicked item
        int index = mGui.getLV().getPositionForView(view);
        // create next activity
        Intent nextActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.entry.Init.class);
        // give entry to next activity and the copy attribute
        nextActivity.putExtra("entry", mAdapter.getItem(index));
        nextActivity.putExtra("copy", true);
        mFragment.getActivity().startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    // gets called by clicking on the edit button of an item
    public void editListViewItem(View view)
    {
        // get the index of the clicked item
        int index = mGui.getLV().getPositionForView(view);
        // create next activity
        Intent nextActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.entry.Init.class);
        // give entry to next activity
        nextActivity.putExtra("entry", mAdapter.getItem(index));
        mFragment.getActivity().startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    // gets called by clicking on the remove button of an item
    public void removeListViewItem(final View view)
    {
        // ask for removing
        new AlertDialog.Builder(mHamburger)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Eintrag löschen")
            .setMessage("Möchten Sie den Eintrag wirklich löschen?")
            .setPositiveButton("Ja", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // get the index of the clicked item
                    int index = mGui.getLV().getPositionForView(view);

                    // remove the item
                    DataStorage.data_object.remove(mAdapter.getItem(index));

                    // update the view
                    mAdapter.notifyDataSetChanged();
                    initGui();
                }

            })
            .setNegativeButton("Nein", null)
            .show();
    }

    // calculate the average calories for the specified days
    public double avgCaloriesForDays(int countDays){
        // set values
        List<Entry> tmpEntries = new ArrayList<Entry>();
        Date date = new Date();
        String dateStr = HelperClass.dateToString(date);
        Date start = HelperClass.stringToDate(dateStr);

        // start calculating one day earlier because today is not count
        Calendar cal = Calendar.getInstance();
        cal.setTime(start);
        cal.add(Calendar.DAY_OF_YEAR, -1);

        // calculate
        start = cal.getTime();
        int count = 0;
        double sum = 0;
        while(count < countDays){
            // adding the calories of each day
            sum = sum + CalorieCalculator.caloriesFromDay(DataStorage.data_object.getEntries(start));
            cal.add(Calendar.DAY_OF_YEAR,-1);
            start = cal.getTime();
            count ++;
        }

        // return the calculated value
        return sum/countDays;
    }

    // init gui on update
    public void onUpdate(){
        initGui();
    }
}
