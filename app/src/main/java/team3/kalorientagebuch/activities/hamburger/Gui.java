package team3.kalorientagebuch.activities.hamburger;

import android.widget.ListView;

import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * Gui class contains references to all Gui elements
 */
public class Gui {

    // link to the Init object extending the Activity
    private Init mActivity;

    // Gui elements
    private ListView mlvMenu;

    // constructor sets the objects
    public Gui (Init activity) {
        mActivity = activity;

        mActivity.setContentView(R.layout.hamburger_activity);

        mlvMenu = (ListView)mActivity.findViewById(R.id.lvMenu);
    }

    // Gui element getter
    public ListView getLVMenu() {
        return mlvMenu;
    }
}
