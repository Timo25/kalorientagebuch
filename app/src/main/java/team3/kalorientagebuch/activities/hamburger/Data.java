package team3.kalorientagebuch.activities.hamburger;

import android.os.Bundle;

import team3.kalorientagebuch.Helpers.CustomFragment;

/**
 * Created by Christopher
 * Data class contains, stores and restores non-persistent data
 */
public class Data {

    // link to the Init object extending the Activity
    private Init mActivity;

    //non-persistent data to be stored
    private int mCurrentFragmentID;
    private CustomFragment mCurrentFragment = null;

    // init or restore data
    public Data (Bundle savedInstanceState, Init activity) {
        mActivity = activity;
        if ( savedInstanceState == null ) {
            mCurrentFragmentID = 0;
        }
        else {
            // restore data if existing
            restoreDataFromBundle(savedInstanceState);
        }
    }

    // save the non-persistent data in the bundle
    public void saveDataInBundle (Bundle b) {
        b.putInt("currentfragmentid", mCurrentFragmentID);
        mActivity.getSupportFragmentManager().putFragment(b, "currentfragment", mCurrentFragment);
    }

    // restore the data of the bundle
    private void restoreDataFromBundle (Bundle b)
    {
        mCurrentFragmentID = b.getInt("currentfragmentid");
        mCurrentFragment = (CustomFragment)mActivity.getSupportFragmentManager().getFragment(b, "currentfragment");
    }

    // getter & setter
    public int getCurrentFragmentID() { return mCurrentFragmentID; }
    public CustomFragment getCurrentFragment() { return mCurrentFragment; }
    public void setCurrentFragmentID(int id) { mCurrentFragmentID = id; setCurrentFragment(null); }
    public void setCurrentFragment(CustomFragment fragment) { mCurrentFragment = fragment; }

    // init the specified fragment
    public void initCurrentFragment()
    {
        if (mCurrentFragment == null)
        {
            switch (mCurrentFragmentID)
            {
                case 0:
                    mCurrentFragment = new team3.kalorientagebuch.activities.home.Init();
                    break;
                case 1:
                    mCurrentFragment = new team3.kalorientagebuch.activities.diary.Init();
                    break;
                case 2:
                    mCurrentFragment = new team3.kalorientagebuch.activities.foodmanagement.Init();
                    break;
                case 3:
                    mCurrentFragment = new team3.kalorientagebuch.activities.unitmanagement.Init();
                    break;
                case 4:
                    mCurrentFragment = new team3.kalorientagebuch.activities.account.Init();
                    break;
            }
        }
    }
}
