package team3.kalorientagebuch.activities.hamburger;

import android.view.View;
import android.widget.AdapterView;

/**
 * Created by Jonas
 * ClickListener class reacts on clicks on the menu
 */
public class ClickListener implements AdapterView.OnItemClickListener {
    // object to execute application logic methods depending on the event
    private ApplicationLogic mApplicationLogic;

    // constructor assigning the ApplicationLogic object
    public ClickListener(ApplicationLogic applicationLogic) {
        mApplicationLogic = applicationLogic;
    }

    // method gets called on click events and calls methods in the ApplicationLogic object depending on the event
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        mApplicationLogic.setFragment(position);
    }
}
