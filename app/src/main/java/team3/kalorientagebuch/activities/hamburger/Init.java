package team3.kalorientagebuch.activities.hamburger;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.Toast;

import team3.kalorientagebuch.Helpers.DataStorage;

/**
 * Created by Jonas
 * the Hamburger menu contains a clickable menu and a container for the content
 */
public class Init extends AppCompatActivity {
    public static final int FRAGMENTS_COUNT = 5;
    public static final int NAVIGATION_CODE_UPDATE = 1;

    // objects of the needed functions
    private Data mData;
    private Gui mGui;
    private ApplicationLogic mApplicationLogic;

    // initialize objects on creation
    @Override
    public void onCreate (Bundle savedInstanceState) {
        // load data on creation
        DataStorage.loadData();

        super.onCreate(savedInstanceState);
        initData(savedInstanceState);
        initGUI();
        initApplicationLogic();
    }

    // init objects
    private void initData (Bundle savedInstanceState) {
        mData = new Data(savedInstanceState, this);
    }
    private void initGUI () {
        mGui = new Gui(this);
    }
    private void initApplicationLogic () {
        mApplicationLogic = new ApplicationLogic(mData, mGui, this);
    }

    // save non-persistent data
    @Override
    protected void onSaveInstanceState (Bundle outState) {
        mData.saveDataInBundle(outState);
        super.onSaveInstanceState(outState);
    }

    // set the first menu state
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mApplicationLogic.getActionBarDrawerToggle().syncState();
    }
    // catch the click on the hamburger icon and forward it to the application logic
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mApplicationLogic.onOptionsItemSelected(item);
        return super.onOptionsItemSelected(item);
    }

    //override the back pressing event tto go always to the home screen
    @Override
    public void onBackPressed() {
        mApplicationLogic.onBackPressed();
    }
    // give fragments the possibility of changing the current fragment
    public void loadSelection(int i)
    {
        mApplicationLogic.setFragment(i);
    }

    // forward updates in form of results to the application logic
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        mApplicationLogic.onFragmentUpdate();
    }

    // Datastorage functions
    // check permissions
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (permissions.length != 0) {
            if (PackageManager.PERMISSION_GRANTED != grantResults[0]) {
                Toast.makeText(getApplicationContext(), "Permission Storage Denied", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        //function to check if permission is granted fro R/W in marshmallow and above
        if (!DataStorage.hasPermission(getApplicationContext())) {
            DataStorage.handlePermission(this);
        }
    }
}