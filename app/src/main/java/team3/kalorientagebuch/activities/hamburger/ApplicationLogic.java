package team3.kalorientagebuch.activities.hamburger;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

import team3.kalorientagebuch.Helpers.DataStorage;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects
    private Data mData;
    private Gui mGui;
    private Init mActivity;

    // objects for the menu
    private ActionBarDrawerToggle mActionBarDrawerToggle;
    private DrawerLayout mDrawerLayout;

    // placeholder objects
    private FragmentManager mFragmentManager;

    // constructor sets the objects and initializes the GUI and the button listener
    public ApplicationLogic (Data data, Gui gui, Init activity) {
        mData = data;
        mGui = gui;
        mActivity = activity;
        initGui();
        initListener();
    }

    private void initGui() {
        // sets the items in the menu
        mDrawerLayout = (DrawerLayout)mActivity.findViewById(R.id.drawerlayout);
        ArrayList<String> array = new ArrayList<String>();
        array.add(mActivity.getString(R.string.hamburgermenu1));
        array.add(mActivity.getString(R.string.hamburgermenu2));
        array.add(mActivity.getString(R.string.hamburgermenu3));
        array.add(mActivity.getString(R.string.hamburgermenu4));
        array.add(mActivity.getString(R.string.hamburgermenu5));
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(mActivity,R.layout.hamburger_listview,array);
        mGui.getLVMenu().setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        mGui.getLVMenu().setAdapter(adapter);

        // configure the action bar
        mActionBarDrawerToggle = new ActionBarDrawerToggle(mActivity, mDrawerLayout,R.string.opendrawer,R.string.closedrawer);
        mDrawerLayout.addDrawerListener(mActionBarDrawerToggle);
        ActionBar actionBar = mActivity.getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        // display the first fragment
        setFragment();
    }

    // creates an object of the ClickListener and bounds it to the menu
    private void initListener() {
        ClickListener clicklistener = new ClickListener(this);
        mGui.getLVMenu().setOnItemClickListener(clicklistener);
    }

    // set the fragment to the specified id
    public void setFragment(int currentFragment){
        //save the specified id in the data object
        mData.setCurrentFragmentID(currentFragment);
        setFragment();
    }
    // set the fragment to the current one
    public void setFragment()
    {
        // set the account view if no data is available
        if ((DataStorage.data_object.getName().length() <= 0 ||
                DataStorage.data_object.getMaxCalories() <= 0) &&
                mData.getCurrentFragmentID() < 4) {
            setFragment(4);
        }

        // set the corresponding ListView item checked
        mGui.getLVMenu().setItemChecked(mData.getCurrentFragmentID(), true);

        // init fragment object
        mData.initCurrentFragment();
        // set the placeholder content depending on the id
        FragmentTransaction mFragmentTransaction;
        mFragmentTransaction = mActivity.getSupportFragmentManager().beginTransaction();
        mFragmentTransaction.replace(R.id.fragmentholder, mData.getCurrentFragment());
        mFragmentTransaction.commit();

        // close the menu
        mDrawerLayout.closeDrawer(mGui.getLVMenu());
    }

    // open and close the hamburger menu by clicking the hamburger button
    public void onOptionsItemSelected(MenuItem item) {
        // get clicked item and check for home
        int id = item.getItemId();
        if (id == android.R.id.home){
            if (mDrawerLayout.isDrawerOpen(mGui.getLVMenu())){
                // close drawer if open
                mDrawerLayout.closeDrawer(mGui.getLVMenu());
            }else{
                // open drawer if closed
                mDrawerLayout.openDrawer(mGui.getLVMenu());

                // change the background color of all elements to transparent
                ListView lvMenu = mGui.getLVMenu();
                for (int i=0; i < lvMenu.getChildCount(); i++)
                {
                    lvMenu.getChildAt(i).setBackgroundColor(0x00000000);
                }
                // change the background color of the current item
                lvMenu.getChildAt(mData.getCurrentFragmentID()).setBackgroundColor(0xffaaaaaa);
            }
        }
    }

    // override the back button
    public void onBackPressed() {
        // set the current fragment to the home screen
        if (mData.getCurrentFragmentID() != 0)
        {
            setFragment(0);
        }
        else
        {
            // kill the process if it is already the home screen
            android.os.Process.killProcess(android.os.Process.myPid());
        }
    }

    // action bar getter
    public ActionBarDrawerToggle getActionBarDrawerToggle() { return mActionBarDrawerToggle; }

    // forward the update to the current fragment
    public void onFragmentUpdate()
    {
        mData.getCurrentFragment().onUpdate();
    }
}
