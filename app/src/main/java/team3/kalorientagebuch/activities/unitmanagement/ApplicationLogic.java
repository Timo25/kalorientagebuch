package team3.kalorientagebuch.activities.unitmanagement;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.View;

import team3.kalorientagebuch.Helpers.DataStorage;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects
    private team3.kalorientagebuch.activities.hamburger.Init mHamburger;
    private Gui mGui;
    private Init mFragment;

    // list of strings for the listview
    private CustomUnitAdapter mAdapter;

    // constructor sets the objects and initializes the GUI and the button listener
    public ApplicationLogic (Gui gui, Init fragment, team3.kalorientagebuch.activities.hamburger.Init hamburger) {
        mGui = gui;
        mFragment = fragment;
        mHamburger = hamburger;
        initGui();
        initListener();
    }

    private void initGui() {
        // change title to current content
        mFragment.getActivity().setTitle(mFragment.getActivity().getString(R.string.hamburgermenu4));

        // set the listview
        mAdapter = new CustomUnitAdapter(mFragment.getActivity(), DataStorage.data_object.getUnits(), new ClickListener(this));
        mGui.getLV().setAdapter(mAdapter);
    }

    // notify about reloading
    public void reloadList()
    {
        mAdapter.notifyDataSetChanged();
    }

    // creates an object of the ClickListener and bounds it to the new unit button
    private void initListener() {
        ClickListener clickListener = new ClickListener(this);
        mGui.getBtnNew().setOnClickListener(clickListener);
    }

    // gets called when the new unit button is clicked and runs the activity to create a new unit
    public void onNewClicked() {
        Intent nextActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.unit.Init.class);
        mFragment.getActivity().startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    // gets called by clicking on the edit button of an item
    public void editListViewItem(View view)
    {
        // get the index of the clicked item
        int index = mGui.getLV().getPositionForView(view);

        Intent nextActivity = new Intent(mFragment.getActivity(), team3.kalorientagebuch.activities.unit.Init.class);
        // give parameters to next activity
        nextActivity.putExtra("unit", mAdapter.getItem(index));
        mFragment.getActivity().startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }
    // gets called by clicking on the remove button of an item
    public void removeListViewItem(final View view)
    {
        // ask for removing
        new AlertDialog.Builder(mHamburger)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setTitle("Einheit löschen")
            .setMessage("Möchten Sie die Einheit wirklich löschen?")
            .setPositiveButton("Ja", new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // get the index of the clicked item
                    int index = mGui.getLV().getPositionForView(view);

                    // remove item and notify adapter
                    DataStorage.data_object.remove(mAdapter.getItem(index));

                    // update the view
                    mAdapter.notifyDataSetChanged();
                }

            })
            .setNegativeButton("Nein", null)
            .show();
    }
}
