package team3.kalorientagebuch.activities.unitmanagement;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.List;

import team3.kalorientagebuch.Objects.Unit;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Jonas
 * Custom adapter to fill the listview in the unit activity with all units
 */
public class CustomUnitAdapter extends BaseAdapter {

    //class variables
    private Context mContext;
    private List<Unit> mList;
    private ClickListener mClickListener;

    //constructor
    public CustomUnitAdapter(Context context, List<Unit> list, ClickListener clickListener){
        mContext = context;
        mList = list;
        mClickListener = clickListener;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Unit getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //Save current item
        Unit curItem = getItem(i);

        //Select layout
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.unitmanagement_listview, null);
        }

        //Fill textviews of each item
        TextView tvName = (TextView) view.findViewById(R.id.tvName);
        tvName.setText(curItem.toString());

        if (curItem.isEditable())
        {
            // set clicklisteners to buttons
            ImageButton btnEdit = (ImageButton)view.findViewById(R.id.btnEdit);
            btnEdit.setVisibility(View.VISIBLE);
            btnEdit.setOnClickListener(mClickListener);
            ImageButton btnDelete = (ImageButton)view.findViewById(R.id.btnDelete);
            btnDelete.setVisibility(View.VISIBLE);
            btnDelete.setOnClickListener(mClickListener);
        }
        else
        {
            // remove buttons
            ImageButton btnEdit = (ImageButton)view.findViewById(R.id.btnEdit);
            btnEdit.setVisibility(View.INVISIBLE);
            ImageButton btnDelete = (ImageButton)view.findViewById(R.id.btnDelete);
            btnDelete.setVisibility(View.INVISIBLE);
        }

        return view;
    }
}
