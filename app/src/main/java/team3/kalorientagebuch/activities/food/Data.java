package team3.kalorientagebuch.activities.food;

import android.os.Bundle;

import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Food;

/**
 * Created by Christopher
 * Data class contains, stores and restores non-persistent data
 */
public class Data {

    // link to the objects
    private Init mActivity;
    private Gui mGui;

    //non-persistent data to be stored
    private Food mFood;
    private boolean mbChange;
    private String msIDBefore;

    // init or restore data
    public Data (Bundle savedInstanceState, Init activity, Gui gui) {
        mActivity = activity;
        mGui = gui;

        if ( savedInstanceState == null ) {

            // check whether a food gets added or edited
            Food intentFood = (Food)mActivity.getIntent().getSerializableExtra("food");
            if (intentFood == null)
            {
                mbChange = false;
                mFood = new Food();
                msIDBefore = "";
            }
            else
            {
                mbChange = true;
                mFood = new Food(intentFood);
                msIDBefore = mFood.getName();
            }
        }
        else {
            restoreDataFromBundle(savedInstanceState);
        }
    }

    // save the non-persistent data in the bundle
    public void saveDataInBundle (Bundle b) {
        mFood.setName(mGui.getMetFoodName().getText().toString());
        mFood.setCalories(HelperClass.stringToInt(mGui.getMetFoodCalories().getText().toString()));

        b.putSerializable("food", mFood);
        b.putBoolean("change", mbChange);
        b.putString("idbefore", msIDBefore);
    }

    // restore the data of the bundle
    private void restoreDataFromBundle (Bundle b) {
        mFood = (Food)b.getSerializable("food");
        mbChange = b.getBoolean("change");
        msIDBefore = b.getString("idbefore", msIDBefore);
    }

    // getter & setter
    public boolean getChange() { return mbChange; }
    public Food getFood() { return mFood; }
    public String getIDBefore() { return msIDBefore; }
}
