package team3.kalorientagebuch.activities.food;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Toast;

import team3.kalorientagebuch.Helpers.DataStorage;
import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Entsprechung;
import team3.kalorientagebuch.Objects.Unit;

/**
 * Created by Saskia
 * ApplicationLogic contains methods that change or process something
 */
public class ApplicationLogic {
    // objects and class variables
    private Data mData;
    private Gui mGui;
    private Init mActivity;

    // list
    private CustomFoodAdapter dataAdapter;

    // spinner
    private ArrayAdapter<Unit> dataAdapterSpinner;

    //Constructor assigning objects and executes further initializing methods
    public ApplicationLogic(Data data, Gui gui, Init activity) {
        mData = data;
        mGui = gui;
        mActivity = activity;
        initGui();
        initListener();
    }

    //Initializes the gui also depending on the call it might prefill values to edit a food
    private void initGui() {
        // change title to current content
        if (mData.getChange())
        {
            mActivity.setTitle("Lebensmittel bearbeiten");
        }
        else
        {
            mActivity.setTitle("Neues Lebensmittel");
        }

        // init spinner
        dataAdapterSpinner = new ArrayAdapter<Unit>(mActivity, android.R.layout.simple_spinner_item, DataStorage.data_object.getUnits());
        dataAdapterSpinner.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mGui.getMspFoodUnit().setAdapter(dataAdapterSpinner);

        //prefill
        mGui.getMetFoodCalories().setText(String.valueOf(mData.getFood().getCalories()));
        mGui.getMetFoodName().setText(mData.getFood().getName());
        mGui.getMlvFood().setAdapter(dataAdapter);

        // init entsprechungen listview
        dataAdapter = new CustomFoodAdapter(mActivity, mData.getFood().getEntsprechungen(), this);
        mGui.getMlvFood().setAdapter(dataAdapter);
    }

    //Updates the spinner when a new unit is created in the "new unit" activity
    public void updateAndSetSpinner(Unit unit)
    {
        dataAdapterSpinner.notifyDataSetChanged();
        mGui.getMspFoodUnit().setSelection(dataAdapterSpinner.getPosition(unit));
    }

    // creates an object of the ClickListener and bounds it to the buttons
    private void initListener() {
        //set button listener
        ClickListener clickListener = new ClickListener(this, null);
        mGui.getMbtnFoodAdd().setOnClickListener(clickListener);
        mGui.getMbtnNewUnit().setOnClickListener(clickListener);
        mGui.getMbtnFoodSave().setOnClickListener(clickListener);

        //When the calorie field is no longer focused the entsprechungen will be recalculated according to the changed calories
        mGui.getMetFoodCalories().setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if(HelperClass.stringToInt(mGui.getMetFoodCalories().getText().toString()) == 0){
                    if (hasFocus)
                    {
                        mGui.getMetFoodCalories().setText("");
                    }
                    else
                    {
                        mGui.getMetFoodCalories().setText("0");
                    }
                }
                if(!hasFocus){
                    updateCalories();
                }
            }
        });
    }

    //Executed when user clicks on the "new unit" button - navigates to the new unit activity
    public void onNewUnitClicked(){
        Intent nextActivity = new Intent(mActivity, team3.kalorientagebuch.activities.unit.Init.class);
        mActivity.startActivityForResult(nextActivity, team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE);
    }

    //Executed when the user clicks on the "add" button
    public void onAddFoodClicked(){
        // get the content of the edittext
        double quantity = HelperClass.stringToDouble(mGui.getMetFoodQty().getText().toString());

        //Validation
        if (quantity <= 0)
        {
            mGui.getMetFoodName().setError("Bitte geben Sie einen Namen ein.");
            return;
        }
        else
        {
            mGui.getMetFoodName().setError(null);
        }

        //delete an entsprechung with the same name
        Unit unit = new Unit((Unit)mGui.getMspFoodUnit().getSelectedItem());
        for (int i = 0; i < mData.getFood().getEntsprechungen().size(); i++)
        {
            if (mData.getFood().getEntsprechungen().get(i).getUnit().equals(unit))
            {
                mData.getFood().getEntsprechungen().remove(i);
                break;
            }
        }

        //Adds an entsprechung to the food object
        mData.getFood().addEntsprechung(new Entsprechung(unit, quantity));
        dataAdapter.notifyDataSetChanged();

        // clear focus after adding
        if (mActivity.getCurrentFocus() != null) {
            mActivity.getCurrentFocus().clearFocus();
        }
    }

    //Saves the food to the db when the users clicks on the "save" button
    public void onSaveFoodClicked() {
        //Validation

        // default: no error
        boolean bError = false;

        String nameStr = mGui.getMetFoodName().getText().toString();
        if(nameStr.length() <= 0){
            // error message
            mGui.getMetFoodName().setError("Bitte geben Sie einen Namen ein.");
            // at least one error
            bError = true;
        }
        else{
            // reset error
            mData.getFood().setName(nameStr);
        }

        String calorieStr = mGui.getMetFoodCalories().getText().toString();
        if(calorieStr.length() <= 0){
            mGui.getMetFoodCalories().setError("Bitte geben Sie eine Kalorienanzahl ein.");
            bError = true;
        }
        else{
            int calories = HelperClass.stringToInt(calorieStr);
            if(calories == 0){
                mGui.getMetFoodCalories().setError("Bitte geben Sie eine positive Kalorienazahl ein.");
                bError = true;
            }
            else{
                mData.getFood().setCalories(calories);
            }
        }

        if(mData.getFood().getEntsprechungen().size() <= 0){
            mGui.getMetFoodQty().setError("Bitte geben Sie mindestens eine Entsprechung an.");
            bError = true;
        }

        //save object
        if (!bError){

            //save data
            boolean bSuccess = true;
            if (mData.getChange())
            {
                DataStorage.data_object.edit(mData.getIDBefore(), mData.getFood());
            }
            else
            {
                bSuccess = DataStorage.data_object.add(mData.getFood());
            }

            //send message and return the object
            if (bSuccess)
            {
                Toast.makeText(mActivity.getApplicationContext(), "Lebensmittel gespeichert!", Toast.LENGTH_SHORT).show();

                Intent data = new Intent();
                data.putExtra("food", mData.getFood());
                mActivity.setResult(team3.kalorientagebuch.activities.hamburger.Init.NAVIGATION_CODE_UPDATE, data);

                mActivity.finish();
            }
            else
            {
                // error message
                Toast.makeText(mActivity.getApplicationContext(), "Lebensmittel existiert bereits!", Toast.LENGTH_SHORT).show();
            }
        }
    }

    //Deletes an entsprechung when the user clicks the delete button on an item in the list of entsprechungen
    public void onDeleteClicked(final Entsprechung entsprechung){
        //Delete warning
        new AlertDialog.Builder(mActivity)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setTitle("Entsprechung löschen")
                .setMessage("Möchten Sie die Entsprechung wirklich löschen?")
                .setPositiveButton("Ja", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mData.getFood().getEntsprechungen().remove(entsprechung);
                        dataAdapter.notifyDataSetChanged();
                    }

                })
                .setNegativeButton("Nein", null)
                .show();
    }

    //Updates the calories and the entsprechungen when the user unfocuses the calories field
    public void updateCalories() {
        
        int caloriesBefore = mData.getFood().getCalories();
        int caloriesAfter = HelperClass.stringToInt(mGui.getMetFoodCalories().getText().toString());
        
        if (caloriesAfter > 0) {
            if (caloriesBefore > 0) {
                if(mData.getFood().getEntsprechungen().size() > 0) {
                    for(Entsprechung ent: mData.getFood().getEntsprechungen()){
                        double qtyBefore = ent.getQuantity();
                        double multiplicator = qtyBefore/caloriesBefore;
                        Double qtyAfter = caloriesAfter*multiplicator;
                        ent.setQuantity(qtyAfter);
                    }
                    dataAdapter.notifyDataSetChanged();
                    mGui.getMlvFood().setAdapter(dataAdapter);
                }
            }
            
            mData.getFood().setCalories(caloriesAfter);
        }
    }
}
