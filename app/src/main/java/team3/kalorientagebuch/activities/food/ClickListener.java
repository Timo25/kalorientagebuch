package team3.kalorientagebuch.activities.food;

import android.view.View;
import team3.kalorientagebuch.Objects.Entsprechung;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Saskia
 * ClickListener class reacts on button clicks
 */
public class ClickListener implements View.OnClickListener {
    // object to execute application logic methods depending on the event
    private ApplicationLogic mApplicationLogic;
    private Entsprechung mEntsprechung;

    // constructor assigning the ApplicationLogic object and the entsprechung object, which is required for clicks on each item
    public ClickListener(ApplicationLogic applicationLogic, Entsprechung entsprechung) {
        mApplicationLogic = applicationLogic;
        mEntsprechung = entsprechung;
    }

    // method gets called on click events and calls methods in the ApplicationLogic object depending on the event
    @Override
    public void onClick(View view) {
        switch ( view.getId() ) {
            case R.id.btnNewUnit:
                mApplicationLogic.onNewUnitClicked();
                break;
            case R.id.btnFoodAdd:
                mApplicationLogic.onAddFoodClicked();
                break;
            case R.id.btnFoodSave:
                mApplicationLogic.onSaveFoodClicked();
                break;
            case R.id.btnEntDelete:
                //Special case, this will fire when the delete button of an item is clicked
                mApplicationLogic.onDeleteClicked(mEntsprechung);
                break;
        }
    }
}