package team3.kalorientagebuch.activities.food;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Saskia
 *
 */
public class Gui {
    // link to the Init object extending the Fragment
    private Init mActivity;

    // Gui elements
    private EditText metFoodName;
    private EditText metFoodCalories;

    private Spinner mspFoodUnit;
    private Button mbtnNewUnit;

    private EditText metFoodQty;

    private Button mbtnFoodAdd;

    private ListView mlvFood;

    private Button mbtnFoodSave;

    // constructor sets the objects
    public Gui(Init activity) {
        mActivity = activity;
        mActivity.setContentView(R.layout.food_activity);

        metFoodName = (EditText) mActivity.findViewById(R.id.etFoodName);
        metFoodCalories = (EditText) mActivity.findViewById(R.id.etFoodCalories);
        mspFoodUnit = (Spinner) mActivity.findViewById(R.id.spUnit);
        mbtnNewUnit = (Button) mActivity.findViewById(R.id.btnNewUnit);
        metFoodQty = (EditText) mActivity.findViewById(R.id.etFoodQty);
        mbtnFoodAdd = (Button) mActivity.findViewById(R.id.btnFoodAdd);
        mlvFood = (ListView) mActivity.findViewById(R.id.lvFood);
        mbtnFoodSave = (Button) mActivity.findViewById(R.id.btnFoodSave);
    }

    // Gui element getter

    public EditText getMetFoodName() {
        return metFoodName;
    }

    public EditText getMetFoodCalories() {
        return metFoodCalories;
    }

    public Spinner getMspFoodUnit() {
        return mspFoodUnit;
    }

    public Button getMbtnNewUnit() {
        return mbtnNewUnit;
    }

    public EditText getMetFoodQty() {
        return metFoodQty;
    }

    public Button getMbtnFoodAdd() {
        return mbtnFoodAdd;
    }

    public ListView getMlvFood() {
        return mlvFood;
    }

    public Button getMbtnFoodSave() {
        return mbtnFoodSave;
    }
}
