package team3.kalorientagebuch.activities.food;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import team3.kalorientagebuch.Helpers.HelperClass;
import team3.kalorientagebuch.Objects.Entsprechung;
import team3.kalorientagebuch.activities.R;

/**
 * Created by Saskia
 * Custom adapter to fill the "Entsprechungen" listview in the "change / create food" activity
 */
public class CustomFoodAdapter extends BaseAdapter {

    //class variables
    private Context mContext;
    private List<Entsprechung> mList = new ArrayList<Entsprechung>();
    private ApplicationLogic mApplicationLogic;

    // Comparator for sorting the array
    private static final Comparator<Entsprechung> comparator = new Comparator<Entsprechung>() {
        public int compare(Entsprechung entsprechung1, Entsprechung entsprechung2) {
            return entsprechung1.toString().compareToIgnoreCase(entsprechung2.toString());
        }
    };

    //constructor
    public CustomFoodAdapter(Context context, List<Entsprechung> list, ApplicationLogic applicationLogic){
        mContext = context;
        mList = list;
        mApplicationLogic = applicationLogic;
        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Entsprechung getItem(int i) {
        return mList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public void notifyDataSetChanged() {
        // sorting before showing
        Collections.sort(mList, comparator);
        super.notifyDataSetChanged();
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        //Save entsprechung of the item
        Entsprechung curEntsprechung = getItem(i);
        //Select layout
        if (view == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = layoutInflater.inflate(R.layout.food_listview_entry, null);
        }
        //Fill textviews of each item
        TextView qty = (TextView) view.findViewById(R.id.tvQty);
        TextView unit = (TextView) view.findViewById(R.id.tvUnit);

        // set texts
        double quantity = curEntsprechung.getQuantity();
        qty.setText(HelperClass.doubleToString(quantity)+" ");
        unit.setText(curEntsprechung.getUnit().toString());

        //Create a clicklistener for each item
        ImageButton btnDelete = (ImageButton) view.findViewById(R.id.btnEntDelete);
        ClickListener cl = new ClickListener(mApplicationLogic, curEntsprechung);
        btnDelete.setOnClickListener(cl);

        return view;
    }
}
