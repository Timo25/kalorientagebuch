package team3.kalorientagebuch.Objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.UUID;

import team3.kalorientagebuch.Helpers.DataStorage;

/**
 * Created by Marcel
 * StoredObject contains all the needed data and can be saved and loaded by the DataStorage
 * Every function, that changes something, calls the save function in the DataStorage
 */

public class StoredObject implements Serializable {

    // needed data
    private String mName;
    private int mMaxCalories;
    private ArrayList<Entry> mEntries;
    private ArrayList<Food> mFoods;
    private ArrayList<Unit> mUnits;

    // Comparator for sorting the array
    private static final Comparator<Object> comparator = new Comparator<Object>() {
        public int compare(Object o1, Object o2) {
            return o1.toString().compareToIgnoreCase(o2.toString());
        }
    };

    // constructor initializes the data lists and variables
    public StoredObject()
    {
        mName = "";
        mMaxCalories = 0;
        mEntries = new ArrayList<Entry>();
        mFoods = new ArrayList<Food>();
        mUnits = new ArrayList<Unit>();
        mUnits.add(new Unit("Gramm", false));
        mUnits.add(new Unit("Esslöffel", false));
        mUnits.add(new Unit("Teelöffel", false));
        mUnits.add(new Unit("Stück", false));
        mUnits.add(new Unit("Milliliter", false));
        mUnits.add(new Unit("Portion", false));
    }

    // set and get the config data
    public void setConfig(String name, int maxcalories) {
        mName = name;
        mMaxCalories = maxcalories;
        DataStorage.saveData();
    }
    public String getName() { return mName; }
    public int getMaxCalories() { return mMaxCalories; }


    // --- Entry functions --- //
    public void add(Entry item)
    {
        // UUID is a unique id in Java so the Entry is unique. the combination of the other values is not always unique
        item.setId(UUID.randomUUID().toString());
        mEntries.add(item);
        DataStorage.saveData();
    }

    public void remove(Entry entry){
        mEntries.remove(entry);
        DataStorage.saveData();
    }

    public void edit(String idbefore, Entry item) {
        // get the position of the old item and set the new one to it
        for(int i = 0; i < mEntries.size(); i++){
            if(mEntries.get(i).getId().equals(idbefore)){
                mEntries.set(i,item);
            }
        }
        DataStorage.saveData();
    }

    public ArrayList<Entry> getEntries()
    {
        return mEntries;
    }

    public ArrayList<Entry> getEntries(Date date)
    {
        // get entries by date returns a copy of the list because it does not contain all entries
        ArrayList<Entry> tmpEntries = new ArrayList<Entry>();

        for (int i = 0; i < mEntries.size(); i++)
        {
            if (date.equals(mEntries.get(i).getDate()))
            {
                tmpEntries.add(mEntries.get(i));
            }
        }

        return tmpEntries;
    }


    // --- Food functions --- //
    public boolean add(Food item)
    {
        // check for item is existing
        if (exist(item))
        {
            return false; // Not saved
        }

        // create new one if not existing
        mFoods.add(item);
        Collections.sort(mFoods, comparator);
        DataStorage.saveData();

        return true; // Saved
    }

    public ArrayList<Food> getFoods()
    {
        return mFoods;
    }

    public void remove(Food item) {
        mFoods.remove(item);
        DataStorage.saveData();
    }

    public void edit(String idbefore, Food item) {
        // get the position of the old item and set the new one to it
        for (int i = 0; i < mFoods.size(); i++) {
            if (mFoods.get(i).getName().equals(idbefore)) {
                mFoods.set(i, item);
            }
        }
        // sort the list names ascending
        Collections.sort(mFoods, comparator);
        DataStorage.saveData();
    }

    private boolean exist(Food item)
    {
        // check whether the name of the item already exists
        for (int i = 0; i < mFoods.size(); i++)
        {
            if (mFoods.get(i).getName().equals(item.getName()))
            {
                return true; // Unit already exists
            }
        }

        return false; // Unit not existing
    }

    public Food getFoodByName(String name){
        for(Food food: mFoods){
            if(food.getName().equals(name)){
                return food;
            }
        }
        return null;
    }


    // --- Unit functions --- //
    public boolean add(Unit item)
    {
        // check for item is existing
        if (exist(item))
        {
            return false; // Not saved
        }

        mUnits.add(item);
        Collections.sort(mUnits, comparator);
        DataStorage.saveData();

        return true; // Saved
    }

    public ArrayList<Unit> getUnits()
    {
        return mUnits;
    }

    public void remove(Unit item) {
        mUnits.remove(item);
        DataStorage.saveData();
    }

    public void edit(String idbefore, Unit item) {
        // get the position of the old item and set the new one to it
        for (int i = 0; i < mUnits.size(); i++) {
            if (mUnits.get(i).getName().equals(idbefore)) {
                mUnits.set(i, item);
            }
        }
        // sort the list names ascending
        Collections.sort(mUnits, comparator);
        DataStorage.saveData();
    }

    private boolean exist(Unit item)
    {
        // check whether the name of the item already exists
        for (int i = 0; i < mUnits.size(); i++)
        {
            if (mUnits.get(i).getName().equals(item.getName()))
            {
                return true; // Unit already exists
            }
        }

        return false; // Unit not existing
    }

    public Unit getUnitByName(String name){
        for(Unit unit: mUnits){
            if(unit.getName().equals(name)){
                return unit;
            }
        }
        return null;
    }
}
