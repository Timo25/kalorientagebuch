package team3.kalorientagebuch.Objects;

import java.io.Serializable;

/**
 * Created by Christopher
 * Object which represents an Entsprechung with the needed attributes, it implements the interface Serializable to be passable from one activity to another
 */
public class Entsprechung implements Serializable{

    private Unit unit;
    private double quantity;

    //Constructor
    public Entsprechung(){

    }

    public Entsprechung(Unit unit, double quantity) {
        this.unit = unit;
        this.quantity = quantity;
    }

    public Entsprechung(Entsprechung entsprechung) {
        this.unit = new Unit(entsprechung.getUnit());
        this.quantity = entsprechung.getQuantity();
    }

    //Getter and Setters
    public Unit getUnit(){
        return this.unit;
    }

    public double getQuantity(){
        return this.quantity;
    }

    public void setUnit(Unit unit){
        this.unit = unit;
    }

    public void setQuantity(double quantity){
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return this.unit.getName();
    }
}
