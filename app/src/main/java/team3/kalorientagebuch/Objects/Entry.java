package team3.kalorientagebuch.Objects;

import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Christopher
 * Object which represents an Entry with the needed attributes, it implements the interface Serializable to be passable from one activity to another
 */
//
public class Entry implements Serializable{

    private String id = "";
    private Date date;
    private Food food;
    private Menu menu;
    private Unit unit;
    private double quantity;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private static DecimalFormat decimalFormat = new DecimalFormat("0.00");

    public Entry(){
        date = new Date();
        food = new Food();
        unit = new Unit();
    }

    //Constructor for entry with just food
    public Entry(Date date, Food food, Unit unit, double quantity) {
        this.date = date;
        this.food = food;
        this.unit = unit;
        this.quantity = quantity;
    }

    //Constructor for entry with just food
    public Entry(String id, Date date, Food food, Unit unit, double quantity) {
        this.id = id;
        this.date = date;
        this.food = food;
        this.unit = unit;
        this.quantity = quantity;
    }

    //Constructor for entry with menu
    public Entry(Date date, Menu menu, Unit unit, double quantity) {
        this.date = date;
        this.menu = menu;
        this.unit = unit;
        this.quantity = quantity;
    }

    //Constructor for entry with entry
    public Entry(Entry entry) {
        this.date = new Date(entry.getDate().getTime());
        this.unit = new Unit(entry.getUnit());
        this.quantity = entry.getQuantity();
        this.food = new Food(entry.getFood());
    }

    //Getter and Setters
    public Date getDate(){
        return this.date;
    }

    public Food getFood(){
        return this.food;
    }

    public Menu getMenu(){
        return this.menu;
    }

    public Unit getUnit(){
        return this.unit;
    }
    public Entsprechung getEntsprechung(){
        if (this.unit != null && this.unit.getName() != null)
        {
            for(Entsprechung entsprechung: this.food.getEntsprechungen()){
                if(this.unit.getName().equals(entsprechung.getUnit().getName())){
                    return entsprechung;
                }
            }
        }

        return null;
    }

    public double getQuantity(){
        return this.quantity;
    }

    public void setDate(Date date){
        this.date = date;
    }

    public void setFood(Food food){
        this.food = food;
    }

    public void setMenu(Menu menu){
        this.menu = menu;
    }

    public void setUnit(Unit unit){
        this.unit = unit;
    }

    public void setQuantity(double quantity){
        this.quantity = quantity;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //Custom toString method to make sure the entry is printed correct
    @Override
    public String toString(){
        if(this.food == null){
            return this.menu.getName() +" "+ decimalFormat.format(this.quantity) +" "+ this.unit;
        }
        return this.food.getName() +" "+ decimalFormat.format(this.quantity) +" "+ this.unit;
    }
}
