package team3.kalorientagebuch.Objects;

import java.io.Serializable;

/**
 * Created by Christopher
 * Object which represents a Unit with the needed attributes, it implements the interface Serializable to be passable from one activity to another
 */
public class Unit implements Serializable{
    private String name;
    private boolean mEditable;

    //Constructor
    public Unit() {
        this.mEditable = true;
    }

    public Unit(String name) {
        this.name = name;
        this.mEditable = true;
    }

    public Unit(String name, boolean editable) {
        this.name = name;
        this.mEditable = editable;
    }

    public Unit(Unit unit) {
        this.name = unit.getName();
        this.mEditable = unit.isEditable();
    }

    //Getter and setter
    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public boolean isEditable(){
        return mEditable;
    }

    public void setEditable(boolean editable){
        this.mEditable = editable;
    }

    @Override
    public String toString() {
        return this.name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Unit unit = (Unit) o;

        return name != null ? name.equals(unit.name) : unit.name == null;
    }

}
