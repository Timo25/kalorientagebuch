package team3.kalorientagebuch.Objects;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Christopher
 * Object which represents a Menu with the needed attributes, it implements the interface Serializable to be passable from one activity to another
 */
public class Menu implements Serializable{

    private String name;
    private List<Food> foodList;
    private int quantity;
    private Unit unit;
    private int calories;

    //Constructor
    public Menu(String name, List<Food> foodList, int quantity, Unit unit, int calories) {
        this.name = name;
        this.foodList = foodList;
        this.quantity = quantity;
        this.unit = unit;
        this.calories = calories;
    }

    //Getter and Setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Food> getFoodList() {
        return foodList;
    }

    public void setFoodList(List<Food> foodList) {
        this.foodList = foodList;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Unit getUnit() {
        return unit;
    }

    public void setUnit(Unit unit) {
        this.unit = unit;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    /*
    This implicates that when adding a menu to the database the food is stored with the correct calories
    Means: The calculation of the quantity of food and the foods calories is done when the menu is added.
     */

    public void updateCalories(){
        int totalCalories = 0;
        for(Food food:this.foodList){
            totalCalories += food.getCalories();
        }
        setCalories(totalCalories);
    }

    //Add and remove food from the menu
    public void addFood(Food food){
        this.foodList.add(food);
    }

    public void removeFood(Food food){
        this.foodList.remove(food);

    }
}
