package team3.kalorientagebuch.Objects;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Christopher
 * Object which represents a Food with the needed attributes, it implements the interface Serializable to be passable from one activity to another
 */
//
public class Food implements Serializable{

    private String name;
    private int calories;
    private List<Entsprechung> entsprechungen;

    public Food(){
        entsprechungen = new ArrayList<Entsprechung>();
    }

    //Constructor
    public Food(String name, int calories, List<Entsprechung> entsprechungen) {
        this.name = name;
        this.calories = calories;
        this.entsprechungen = entsprechungen;
    }

    public Food(Food food) {
        this.name = food.getName();
        this.calories = food.getCalories();
        this.entsprechungen = new ArrayList<Entsprechung>();
        for (int i = 0; i < food.getEntsprechungen().size(); i++)
        {
            this.entsprechungen.add(new Entsprechung(food.getEntsprechungen().get(i)));
        }
    }

    //Getter and Setters
    public String getName(){
        return this.name;
    }

    public int getCalories(){
        return this.calories;
    }

    public List<Entsprechung> getEntsprechungen(){
        return this.entsprechungen;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setCalories(int calories){
        this.calories = calories;
    }

    public void setEntsprechungen(List<Entsprechung> entsprechungen){
        this.entsprechungen = entsprechungen;
    }

    public void addEntsprechung(Entsprechung entsprechung){
        this.entsprechungen.add(entsprechung);
    }

    public void removeEntsprechung(Entsprechung entsprechung){
        this.entsprechungen.remove(entsprechung);
    }

    @Override
    public String toString() {
        return this.name;
    }
}
