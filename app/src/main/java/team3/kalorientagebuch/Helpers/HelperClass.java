package team3.kalorientagebuch.Helpers;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Jonas
 * Some functions for the other classes because of redundancy
 */

public class HelperClass {
    // Formats
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private static final DecimalFormat decimalFormat = new DecimalFormat("#.##");


    // self explaining simple converting functions //

    public static int stringToInt(String s) {
        try
        {
            return Integer.parseInt(s);
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public static String doubleToString(double d) {
        return decimalFormat.format(d);
    }
    public static double stringToDouble(String s)
    {
        try
        {
            return Double.parseDouble(s);
        }
        catch (Exception ex)
        {
            return 0;
        }
    }
    public static Date stringToDate(String s) {
        try
        {
            return dateFormat.parse(s);
        } catch (Exception e) {
            return null;
        }
    }
    public static String dateToString(Date d) {
        try
        {
            return dateFormat.format(d);
        } catch (Exception e) {
            return "";
        }
    }
}
