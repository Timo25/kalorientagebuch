package team3.kalorientagebuch.Helpers;

/**
 * Created by Marcel
 * The DataStorage class provides the StoredObject and the functions to save it
 */

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.content.ContextCompat;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import team3.kalorientagebuch.Objects.StoredObject;

public class DataStorage {

    // Permission constant
    private static final int PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE = 1;

    // static data_object containing all information
    public static StoredObject data_object = new StoredObject();

    public static void saveData() {
        try
        {
            // try serializing the object to the sdcard
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(new File("/sdcard/save_object.bin")));
            oos.writeObject(data_object);
            oos.flush();
            oos.close();
        }
        catch(Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public static void loadData()
    {
        try
        {
            // try deserializing the object from the sdcard
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream(new File("/sdcard/save_object.bin")));
            data_object = (StoredObject)ois.readObject();
        }
        catch(Exception ex)
        {
            // on error, create new object
            data_object = new StoredObject();
        }
    }

    // functions for handling the permissions to save and read something to and from the sdcard
    public static boolean hasPermission(Context context) {
        return PackageManager.PERMISSION_GRANTED ==
                ContextCompat.checkSelfPermission(context,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }
    public static void handlePermission(Activity activity) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            activity.requestPermissions(new String[]{
                    Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    PERMISSIONS_REQUEST_WRITE_EXTERNAL_STORAGE);
        }
    }
}