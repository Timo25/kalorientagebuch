package team3.kalorientagebuch.Helpers;

import android.app.Activity;
import android.support.v4.app.Fragment;

/**
 * Created by Jonas
 *
 */

public class CustomFragment extends Fragment {

    // knows the hamburger activity
    protected team3.kalorientagebuch.activities.hamburger.Init mHamburger;

    // empty constructor for creating Fragment
    public CustomFragment() {
    }

    // gets called to updates the fragment
    public void onUpdate()
    {}

    // sets the reference to the hamburger activity
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mHamburger = (team3.kalorientagebuch.activities.hamburger.Init)activity;
    }
}
