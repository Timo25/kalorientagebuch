package team3.kalorientagebuch.Helpers;

import java.util.List;

import team3.kalorientagebuch.Objects.Entry;
import team3.kalorientagebuch.Objects.Entsprechung;
import team3.kalorientagebuch.Objects.Food;

/**
 * Created by Timo
 * Class which calculates calories for a day or an entry
 */
public class CalorieCalculator {

    //returns the calories of an entry
    public static Double caloriesFromEntry(Entry entry){
        double entryCalories = 0.0;
        if(entry.getFood() != null){
            Food food = entry.getFood();
            double defaultQty = 0;
            List<Entsprechung> entsprechungList = food.getEntsprechungen();
            for(int i = 0; i < entsprechungList.size(); i++){
                if(entsprechungList.get(i).getUnit().equals(entry.getUnit())){
                    defaultQty = entsprechungList.get(i).getQuantity();
                }
            }
            if(defaultQty == 0){
                return entryCalories;
            }
            double entryQty = entry.getQuantity();
            //get the exact multiplicator
            double multiplicator = entryQty/defaultQty;
            entryCalories = food.getCalories()*multiplicator;
            return entryCalories;
        }
        else{
            return entryCalories;
        }
    }

    //returns the total calories of a day
    public static double caloriesFromDay(List<Entry> entryList){
        double totalDayCalories = 0.0;
        for(Entry entry: entryList){
            totalDayCalories += caloriesFromEntry(entry);
        }
        return totalDayCalories;
    }
}
